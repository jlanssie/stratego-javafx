package Stratego.View;

import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Screen;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class UISettings {

    private int resX;
    private int resY;
    private int insetsMargin;
    public static final char FILE_SEPARATOR = System.getProperties().getProperty("file.separator").charAt(0);
    private String ApplicationName;
    private String homeDir;
    private String defaultCss = "style.css";
    private Path styleSheetPath = Paths.get("resources"+FILE_SEPARATOR+"stylesheets"+FILE_SEPARATOR+defaultCss);
    private Path AboutImagePath = Paths.get("resources"+FILE_SEPARATOR+"images"+FILE_SEPARATOR+"AboutImage.png");
    private Path applicationIconPath = Paths.get("resources"+FILE_SEPARATOR+"images"+FILE_SEPARATOR+"icons"+FILE_SEPARATOR+"icon3.png");
    private Path startScreenImagePath = Paths.get("resources"+FILE_SEPARATOR+"images"+FILE_SEPARATOR+"logos"+FILE_SEPARATOR+"logo3.png");
    private Path boardBackgroundImagePath = Paths.get("resources"+FILE_SEPARATOR+"images"+FILE_SEPARATOR+"background.jpg");
    private Path infoTextPath = Paths.get("resources"+FILE_SEPARATOR+"other"+FILE_SEPARATOR+"info.txt");

    public UISettings() {
        this.resX= (int) Screen.getPrimary().getVisualBounds().getWidth();
        this.resY = (int) Screen.getPrimary().getVisualBounds().getHeight();
        this.insetsMargin = this.getLowestRes()/100;
        this.homeDir = System.getProperties().getProperty("user.dir");
        this.ApplicationName = "Stratego";
    };

    public int getResX () {return this.resX;}

    public int getResY () {return this.resY;}

    public int getInsetsMargin () {return this.insetsMargin;}

    public int getLowestRes () {return (resX>resY?resX:resY);}

    public boolean styleSheetAvailable (){return Files.exists(styleSheetPath);}

    public Path getStyleSheetPath () {return this.styleSheetPath;}

    public void setStyleSheetPath (Path styleSheetPath) {this.styleSheetPath = styleSheetPath;}

    public String getHomeDir () {return this.homeDir;}

    public Path getApplicationIconPath () {return this.applicationIconPath;}

    public Path getStartScreenImagePath () {return this.startScreenImagePath;}

    public Path getBoardBackgroundImagePath () {return this.boardBackgroundImagePath;}

    public Path getAboutImagePath () {return this.AboutImagePath;}

    public Path getPawnImagePath (int i) {

        Path pawnImagePath = Paths.get("resources"+FILE_SEPARATOR+"images"+FILE_SEPARATOR+"pawns"+FILE_SEPARATOR+"pawn"+i+ ".png");

        return pawnImagePath;
    }

    public Path getMaskedPawnImagePath () {

        Path pawnImagePath = Paths.get("resources"+FILE_SEPARATOR+"images"+FILE_SEPARATOR+"pawns"+FILE_SEPARATOR+"mask.png");

        return pawnImagePath;
    }


    public Path getInfoTextPath () {return this.infoTextPath;}

    public String getApplicationName () {return this.ApplicationName;}

    public static Font font(int size, FontWeight fontWeight){
        return Font.font("Helvetica", fontWeight, size);
    }

}
