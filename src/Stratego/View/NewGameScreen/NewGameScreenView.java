package Stratego.View.NewGameScreen;

import Stratego.View.UISettings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.control.*;

public class NewGameScreenView extends VBox {

    private Button startGameBtn;
    private Button cancelBtn;
    private Label nameLbl;
    private TextField nameTxtField;
    private Label pawnPlacementLbl;
    private ListView<String> pawnPlacementList;
    private ObservableList<String> pawnPlacementListItems;
    private GridPane fields;
    private UISettings uiSettings;

    public NewGameScreenView(UISettings uiSettings) {
        this.uiSettings = uiSettings;
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        startGameBtn = new Button("Start game");
        cancelBtn = new Button("Cancel");
        fields = new GridPane();
        nameLbl = new Label("Your name:");
        nameTxtField = new TextField("");
        pawnPlacementLbl = new Label("How do you want to place your pawns?");
        pawnPlacementList = new ListView<>();
        pawnPlacementListItems = FXCollections.observableArrayList ("Smart Random", "Random", "Load Setup");
    }

    private void layoutNodes() {
        //Button disabled
        startGameBtn.setDisable(true);

        //Set up the grid pane
        GridPane.setConstraints(nameLbl, 0, 0);
        fields.add(nameLbl, 0, 0);

        GridPane.setConstraints(nameTxtField, 1,0);
        fields.add(nameTxtField, 1,0);

        GridPane.setConstraints(pawnPlacementLbl, 0, 1);
        fields.add(pawnPlacementLbl, 0, 1);

        double d = this.getHeight();
        pawnPlacementList.setItems(pawnPlacementListItems);
        int rowHeight = 26;
        int nbItemsInList = pawnPlacementListItems.size();
        pawnPlacementList.setPrefHeight( nbItemsInList * rowHeight);
        GridPane.setConstraints(pawnPlacementList, 1, 1, 1, nbItemsInList);
        fields.add(pawnPlacementList,1, 1);
        fields.setHgap(10);
        fields.setVgap(10);

        HBox buttonsHBox = new HBox(startGameBtn, cancelBtn);
        buttonsHBox.setAlignment(Pos.CENTER);
        buttonsHBox.setSpacing(uiSettings.getInsetsMargin()/3);

        //Add objects to the VBOX
        this.getChildren().addAll(fields, buttonsHBox);
        this.setAlignment(Pos.CENTER);
        this.setSpacing(10);
        this.setPadding(new Insets(uiSettings.getInsetsMargin()));

    }

    TextField getNameTxtField() {
        return nameTxtField;
    }

    ListView<String> getPawnPlacementList() {
        return pawnPlacementList;
    }

    Button getStartGameBtn() {
        return startGameBtn;
    }

    Button getCancelBtn() {
        return cancelBtn;
    }
}
