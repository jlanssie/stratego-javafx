package Stratego.View.NewGameScreen;

import Stratego.Model.*;
import Stratego.View.MainScreen.MainScreenPresenter;
import Stratego.View.UISettings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class NewGameScreenPresenter {

    private Model model;
    private NewGameScreenView view;
    private UISettings uiSettings;
    private boolean nameFilledIn = false;
    private boolean placementMethodSelected = false;
    private File file;

    public NewGameScreenPresenter(Model model, NewGameScreenView view, UISettings uiSettings) {
        this.model = model;
        this.view = view;
        this.uiSettings = uiSettings;
        this.addEventHandlers();
        this.updateView();
    }

    private void addEventHandlers() {
        view.getNameTxtField().setOnKeyTyped(event -> {
            if (view.getNameTxtField().getText().isEmpty())
                nameFilledIn = false;
            else
                nameFilledIn = true;
            updateView();
        });

        view.getPawnPlacementList().getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if(newValue.equalsIgnoreCase("load setup")){
                    file = getFile();
                    if(file != null) {
                        placementMethodSelected = true;
                    }else{
                        view.getPawnPlacementList().getSelectionModel().clearSelection();
                        placementMethodSelected = false;
                    }
                }
                else{
                    placementMethodSelected = true;
                }
                updateView();
            }
        });

        view.getStartGameBtn().setOnMouseClicked(mouseEvent -> {
            String playerName = view.getNameTxtField().getText();
            PawnPlacement placementMethod = PawnPlacement.getPlacementType(view.getPawnPlacementList().getSelectionModel().getSelectedItem());

            if( placementMethod != PawnPlacement.LOADSETUP)
                Model.startNewGame(playerName, placementMethod);
            else{
                Model.startNewGame(playerName, placementMethod, file);
            }
            MainScreenPresenter.setManualSetupPhase(true);
            MainScreenPresenter.setGameStarted(true);
            MainScreenPresenter.setNewGameCancelled(false);
            view.getScene().getWindow().hide();
        });

        view.getCancelBtn().setOnMouseClicked(mouseEvent -> {
            MainScreenPresenter.setNewGameCancelled(true);
            view.getScene().getWindow().hide();
        });
    }

    private File getFile(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Load Setup File");
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("Textfiles", "*.txt"));

        File selectedFile = fileChooser.showOpenDialog(view.getScene().getWindow());
        if ((selectedFile != null) ){
            if(!PawnSetup.isValidSetupFile(selectedFile)){
                selectedFile = null;
                displayAlert("Problem with the selected input file:", "File is corrupted");
            }
        } else {
            displayAlert("Problem with the selected input file:", "File is not readable");
        }
        return selectedFile;
    }

    private void displayAlert(String headerText, String contentText){
        Alert errorWindow = new Alert(Alert.AlertType.ERROR);
        errorWindow.setHeaderText(headerText);
        errorWindow.setContentText(contentText);
        errorWindow.showAndWait();
    }

    private void updateView() {
        if(nameFilledIn && placementMethodSelected)
            view.getStartGameBtn().setDisable(false);
        else
            view.getStartGameBtn().setDisable(true);
    }
}
