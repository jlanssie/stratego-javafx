package Stratego.View.MainScreen;

import Stratego.Model.Player;
import Stratego.View.MainScreen.Layout.*;
import Stratego.View.UISettings;
import com.sun.javafx.iio.gif.GIFDescriptor;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Pos;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.Duration;

import static Stratego.Model.Player.*;

public class MainScreenView extends BorderPane  {

    private MenuItem exitMI;
    private MenuItem aboutMI;
    private MenuItem infoMI;
    private MenuItem newGame;
    private UISettings uiSettings;
    private Stage primaryStage;

    private PlayerSection computerPlayerSection;
    private PlayerSection playerSection;
    private BoardSection boardSection;
    private BorderPane scoreSection;
    private VBox wrapper;
    private VBox wrapperRight;
    private Button manualSetupButton;
    private Label manualSetupInfo;
    private Label manualBoardSetupInfo;
    private Separator separator1;
    private Separator separator2;
    private Separator separator3;
    private Label scoreTitle;
    private HBox scoreWrapper;
    private Label scoreComputer;
    private Label scorePlayer;

    public MainScreenView(UISettings uiSettings, Stage primaryStage) {
        this.uiSettings = uiSettings;
        this.primaryStage = primaryStage;
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        this.exitMI = new MenuItem("Exit");
        this.aboutMI = new MenuItem("About");
        this.infoMI = new MenuItem("Info");
        this.newGame = new MenuItem("New Game");

        this.wrapper = new VBox(10);
        wrapper.getStyleClass().add("wrapper");
        wrapper.prefWidthProperty().bind(primaryStage.widthProperty());
        wrapper.prefHeightProperty().bind(primaryStage.heightProperty());
        wrapper.setMaxWidth(600);

        this.computerPlayerSection = new PlayerSection(uiSettings, PlaceOnBoard.TOP);
        computerPlayerSection.getStyleClass().add("playerSection");
        computerPlayerSection.setId("computerPlayerSection");

        this.playerSection = new PlayerSection(uiSettings, PlaceOnBoard.BOTTOM);
        playerSection.getStyleClass().add("playerSection");
        playerSection.setId("playerSection");

        boardSection = new BoardSection(uiSettings);
        boardSection.getStyleClass().add("boardSection");
        boardSection.prefWidthProperty().bind(wrapper.widthProperty());
        boardSection.setMaxWidth(600);

        this.wrapperRight = new VBox();
        wrapperRight.prefHeightProperty().bind(primaryStage.heightProperty());

        this.manualSetupButton = new Button();
        manualSetupButton.getStyleClass().add("button");
        manualSetupButton.setText("Finish setup");
        manualSetupButton.setVisible(false);

        this.manualBoardSetupInfo = new Label();
        manualBoardSetupInfo.getStyleClass().add("infoText");
        manualBoardSetupInfo.setText("Board setup");
        manualBoardSetupInfo.setAlignment(Pos.CENTER);
        manualBoardSetupInfo.setVisible(false);

        this.manualSetupInfo = new Label();
        manualSetupInfo.getStyleClass().add("infoText");
        manualSetupInfo.setText("Tap two pieces to swap");
        manualSetupButton.setAlignment(Pos.CENTER);
        manualSetupInfo.setVisible(false);

        wrapperRight = new VBox();

        scoreTitle = new Label();
        scoreTitle.getStyleClass().add("scoreTitle");
        scoreTitle.setText("SCORE");

        scoreWrapper = new HBox();
        scoreWrapper.getStyleClass().add("scoreWrapper");


        scoreComputer = new Label();
        scoreComputer.getStyleClass().add("scoreLabel");
        scoreComputer.setId("computerScore");
        scoreComputer.setText("0");

        scorePlayer = new Label();
        scorePlayer.getStyleClass().add("scoreLabel");
        scorePlayer.setId("playerScore");
        scorePlayer.setText("0");

        this.separator1 = new Separator();
        separator1.getStyleClass().add("divider");
        this.separator2 = new Separator();
        separator2.getStyleClass().add("divider");
        this.separator3 = new Separator();
        separator3.getStyleClass().add("scoreDivider");
        separator3.setVisible(false);
    }

    private void layoutNodes() {
        Menu menuFile = new Menu("File",null, newGame, exitMI);
        Menu menuHelp = new Menu("Help",null, aboutMI, infoMI);
        MenuBar menuBar = new MenuBar(menuFile,menuHelp);
        setTop(menuBar);

        this.setCenter(wrapper);
        wrapper.getChildren().add(computerPlayerSection);
        wrapper.getChildren().add(separator1);
        wrapper.getChildren().add(boardSection);
        wrapper.getChildren().add(separator2);
        wrapper.getChildren().add(playerSection);

        this.setRight(wrapperRight);
        scoreWrapper.getChildren().addAll(scoreComputer, scorePlayer);
        scoreWrapper.setAlignment(Pos.BASELINE_CENTER);
        scoreWrapper.setSpacing(10);

        wrapperRight.getChildren().addAll(scoreTitle, scoreWrapper, separator3,manualBoardSetupInfo, manualSetupInfo, manualSetupButton);
        wrapperRight.setAlignment(Pos.CENTER);
    }

    MenuItem getExitItem() {return exitMI;}

    MenuItem getAboutItem() {return aboutMI;}

    MenuItem getInfoItem() {return infoMI;}

    MenuItem getNewGame() {return newGame;}

    BoardSection getBoardSection() {return boardSection;}

    PlayerSection getComputerPlayerSection() {return computerPlayerSection;};

    PlayerSection getPlayerSection() {return playerSection;};

    Button getManualSetupButton() {return manualSetupButton;}

    void setComputerScore(int i){
        scoreComputer.setText(String.valueOf(i));
    }

    void setPlayerScore(int i){
        scorePlayer.setText(String.valueOf(i));
    }

    void isManualSetupButtonVisible(Boolean visible){
        if(visible){
            separator3.setVisible(true);
            manualSetupButton.setVisible(true);
            manualSetupInfo.setVisible(true);
            manualBoardSetupInfo.setVisible(true);
        } else {
            separator3.setVisible(false);
            manualSetupButton.setVisible(false);
            manualSetupInfo.setVisible(false);
            manualBoardSetupInfo.setVisible(false);
        }
    }
}
