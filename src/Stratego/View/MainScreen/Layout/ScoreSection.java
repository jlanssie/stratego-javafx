package Stratego.View.MainScreen.Layout;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;

public class ScoreSection extends BorderPane {
    private Label label;

    public ScoreSection() {
        this.initialiseNodes();
        this.layoutNodes();
    }

    private void initialiseNodes() {
        this.label = new Label("Score");
        label.setId("score");
    }

    private void layoutNodes() {
        this.setCenter(label);
        this.setAlignment(label, Pos.CENTER);
    }
}
