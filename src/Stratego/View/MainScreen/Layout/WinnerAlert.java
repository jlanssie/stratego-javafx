package Stratego.View.MainScreen.Layout;

import Stratego.Model.Color;
import Stratego.Model.ComputerPlayer;
import Stratego.Model.Player;
import javafx.scene.control.Alert;

public class WinnerAlert extends Alert {
    public WinnerAlert(AlertType alertType, Player winner) {
        super(alertType.INFORMATION);

        String name = winner.getName();
        String title = new String();
        String headerText = new String();
        String contextText = new String();
        if(winner instanceof ComputerPlayer ){
            title = "You lose!";
            contextText = "Better luck next time!";
        } else {
            title = "You win!";
            contextText = "Congratulations, " + winner.getName() + ". You won!";
        }

        this.setTitle(title);
        this.setHeaderText(headerText);
        this.setContentText(contextText);
    }
}
