package Stratego.View.MainScreen.Layout;

import Stratego.Model.Player;
import Stratego.View.UISettings;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import jdk.jfr.BooleanFlag;

import java.net.MalformedURLException;

public class PlayerSection extends GridPane {
    private ImageView pawnImageView;
    private Image pawnImage;
    private UISettings uiSettings;
    private Label[][] pawnCounter;

    public PlayerSection(UISettings uiSettings, Player.PlaceOnBoard topOrBottom) {
        this.uiSettings = uiSettings;
        this.pawnCounter = new Label[2][10];
        this.nodes(topOrBottom);
    }

    private void nodes(Player.PlaceOnBoard topOrBottom) {
        for(int i = 0; i < pawnCounter.length; i++) {
            for (int j = 0; j < pawnCounter[i].length; j++) {
                if (((topOrBottom == Player.PlaceOnBoard.TOP) && (i == 1)) || ((topOrBottom == Player.PlaceOnBoard.BOTTOM) && (i == 0))) {
                    pawnCounter[i][j] = new Label();
                    pawnCounter[i][j].prefWidthProperty().bind(this.widthProperty().divide(10));
                    //pawnCounter[i][j].prefHeightProperty().bind(this.heightProperty());
                    pawnCounter[i][j].setMaxWidth(60);
                    pawnCounter[i][j].setMaxHeight(60);
                    pawnCounter[i][j].setMinWidth(60);
                    pawnCounter[i][j].setMinHeight(60);

                    try {
                        pawnImage = new Image(uiSettings.getPawnImagePath(j + 1).toUri().toURL().toString());
                    } catch (MalformedURLException e) {
                        //Nothing to do, if image not found, nothing is shown
                    }

                    pawnImageView = new ImageView(pawnImage);
                    pawnImageView.setFitWidth(54);
                    pawnImageView.setPreserveRatio(true);

                    pawnCounter[i][j].setGraphic(pawnImageView);
                    pawnCounter[i][j].getStyleClass().add("playerSectionTile");

                    this.add(pawnCounter[i][j], j + 1, i + 1);
                } else {
                    pawnCounter[i][j] = new Label();
                    pawnCounter[i][j].setText("0");
                    this.add(pawnCounter[i][j], j + 1, i + 1);
                }
            }
        }
    }

    public Label[][] getPawnCounter(){
        return pawnCounter;
    }

    public void setAmount(Boolean computerPlayer, int rank, int amount){
        int startingRow = (computerPlayer ? 0 : 1);

        pawnCounter[startingRow][rank].setText(String.valueOf(amount));
    }
}
