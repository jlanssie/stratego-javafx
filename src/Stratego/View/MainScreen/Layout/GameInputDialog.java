package Stratego.View.MainScreen.Layout;

import javafx.scene.control.ChoiceDialog;

import java.util.Collection;

public class GameInputDialog extends ChoiceDialog<String> {
    public GameInputDialog(String string, Collection<String> stringCollection) {
        super(string, stringCollection);
        this.setTitle(null);
        this.setHeaderText(null);
        this.setGraphic(null);
        this.setContentText("Place pawns:");
        //this.setHeaderText("Select your choice");
    }
}