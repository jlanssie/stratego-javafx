package Stratego.View.MainScreen.Layout;

import Stratego.Model.Color;
import Stratego.View.UISettings;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import java.net.MalformedURLException;

public class BoardSection extends GridPane {
    private ImageView tileImageView;
    private Image tileImage;
    private BoardTile[][] board;
    private UISettings uiSettings;
    private static String PAWN_COLOR_RED = "#ff8080";
    private static String PAWN_COLOR_BLUE = "#8080ff";

    public BoardSection(UISettings uiSettings) {
        this.uiSettings = uiSettings;
        this.board = new BoardTile[10][10];
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                board[i][j] = new BoardTile(i, j);

                board[i][j].prefWidthProperty().bind(this.widthProperty());
                board[i][j].prefHeightProperty().bind(board[i][j].prefWidthProperty());
                board[i][j].setMaxWidth(60);
                board[i][j].setMaxHeight(60);
                board[i][j].setMinWidth(60);
                board[i][j].setMinHeight(60);

                if( (i == 4 || i == 5) && (j == 2 || j == 3 || j == 6 || j ==7)) {
                    board[i][j].getStyleClass().add("waterTile");
                } else {
                    board[i][j].getStyleClass().add("landTile");
                }

                this.add(board[i][j], j + 1, i + 1);
            }
        }
    }

    private void layoutNodes () {

    }

    public BoardTile[][] getBoard() {
        return board;
    }

    public void setPawnImage(int rank, int row, int col){
        try {
            tileImage = new Image(uiSettings.getPawnImagePath(rank).toUri().toURL().toString());
        } catch (MalformedURLException e) {
            //Nothing to do, if image not found, nothing is shown
        }

        setPawnImage(tileImage, row, col);
    }

    public void setMaskedImage(int row, int col){
        try {
            tileImage = new Image(uiSettings.getMaskedPawnImagePath().toUri().toURL().toString());
        } catch (MalformedURLException e) {
            //Nothing to do, if image not found, nothing is shown
        }

        setPawnImage(tileImage, row, col);
    }

    public void setNoImage(int row, int col){
        setImage(null, row, col);
    }

    private void setPawnImage(Image image, int row, int col){
        tileImageView = new ImageView(tileImage);
        tileImageView.setFitWidth(54);
        tileImageView.setPreserveRatio(true);

        setImage(tileImageView, row, col);
    }

    private void setImage(ImageView imageView, int row, int col){
        board[row][col].setGraphic(imageView);
    }

    public void setColor(int row, int col, Stratego.Model.Color color){
        StringBuilder s = new StringBuilder();
        String setStyle = "-fx-background-color: ";
        s.append(setStyle);

        if(color == Color.RED){
           s.append(PAWN_COLOR_RED);
        } else if (color == Color.BLUE) {
            s.append(PAWN_COLOR_BLUE);
        } else {
            s.append("#f8f8f8");
        }

        board[row][col].setStyle(s.toString());
    }

    public void setBorderAroundTile(int row, int col, Color pawnColor){
        String borderStyle = "-fx-border-color: green";
        String backgroundColor = "-fx-background-color: ";
        if(pawnColor == Color.RED)
            backgroundColor += PAWN_COLOR_RED;
        else
            backgroundColor += PAWN_COLOR_BLUE;
        board[row][col].setStyle(borderStyle + ";" + backgroundColor + "; -fx-border-width: 3px");
    }

    public void setNoBorderAroundTile(int row, int col, Color pawnColor){
        String backgroundColor = "-fx-background-color: ";
        if(pawnColor == Color.RED)
            backgroundColor += PAWN_COLOR_RED;
        else
            backgroundColor += PAWN_COLOR_BLUE;
        board[row][col].setStyle(backgroundColor);
    }

}
