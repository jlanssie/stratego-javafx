package Stratego.View.MainScreen.Layout;

import javafx.scene.control.ButtonType;
import javafx.scene.control.TextInputDialog;

public class NameInputDialog extends TextInputDialog {

    public NameInputDialog() {
        this.setTitle(null);
        this.setHeaderText(null);
        this.setGraphic(null);
        this.setContentText("Your name:");
        this.getDialogPane().lookupButton(ButtonType.CANCEL).setVisible(false);
    }
}
