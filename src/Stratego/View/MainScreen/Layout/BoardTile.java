package Stratego.View.MainScreen.Layout;

import Stratego.Model.Pawn;
import javafx.scene.control.Label;

public class BoardTile extends Label {
    private int[] coordinates;

    public BoardTile(int row, int col) {
        coordinates = new int[2];
        coordinates[0] = row;
        coordinates[1] = col;
    }

    public int[] getCoordinates(){
        return coordinates;
    }
}
