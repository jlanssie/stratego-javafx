package Stratego.View.MainScreen.Layout;

import Stratego.Model.ComputerPlayer;
import Stratego.Model.Player;
import javafx.scene.control.Alert;

/**
 * @author Mante Bos
 * @version 1.0 14/03/2020 09:52 *
 */
public class TooMuchSameMovesAlert extends Alert {

    public TooMuchSameMovesAlert() {
        super(AlertType.WARNING);

        String title = "Illegal Move";
        String headerText = "You made this move too many times";
        String contextText = "A pawn can only move maximum 5 times in a row back and forth. Please select another move.";

        this.setTitle(title);
        this.setHeaderText(headerText);
        this.setContentText(contextText);
    }
}
