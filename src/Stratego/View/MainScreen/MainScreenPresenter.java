package Stratego.View.MainScreen;

import Stratego.Model.*;
import Stratego.Model.Exceptions.IllegalMoveException;
import Stratego.Model.Exceptions.IllegalTileSelection;
import Stratego.View.AboutScreen.*;
import Stratego.View.InfoScreen.*;
import Stratego.View.MainScreen.Layout.*;
import Stratego.View.NewGameScreen.*;
import Stratego.View.UISettings;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.*;
import javafx.scene.*;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.*;
import javafx.util.Duration;

import java.net.MalformedURLException;
import java.nio.file.Files;

import static java.lang.Thread.currentThread;
import static java.lang.Thread.sleep;

public class MainScreenPresenter {

    private Model model;
    private MainScreenView view;
    private UISettings uiSettings;

    Move move;
    Move computerMove;
    Move switchMove;
    boolean originSelected;
    boolean switchOriginSelected;

    private static Boolean manualSetupPhase = true;
    private static boolean gameStarted = false;
    private static boolean newGameCancelled = false;
    public enum Events {START, REVEAL, PLAYERMOVE, COMPUTERMOVEREVEAL, WAIT, COMPUTERMOVERESULT}
    private Events eventToHandle;

    public MainScreenPresenter(Model model, MainScreenView view, UISettings uiSettings) {
        this.model = model;
        this.view = view;
        this.uiSettings = uiSettings;

        this.originSelected = false;
        this.switchOriginSelected = false;

        updateView();
        EventHandlers();

        eventToHandle = Events.START;

        Timeline eventFirer = new Timeline(new KeyFrame(Duration.millis(600), new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                handleEvents(event);
            }
        }));

        eventFirer.setCycleCount(Timeline.INDEFINITE);
        eventFirer.play();
    }

    private void EventHandlers() {
        view.getExitItem().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                handleCloseEvent(event);
            }
        });

        view.getAboutItem().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                handleAboutEvent();
            }});

        view.getInfoItem().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                handleInfoEvent();
            }});

        view.getNewGame().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                showNewGameWindow();
            }
        });

        view.getBoardSection().getChildren().forEach(item -> {
            item.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if(!gameStarted)
                        return;

                    if(manualSetupPhase){

                        manualSetupMove(event, (BoardTile) item);

                    } else {

                        playerMove(event, (BoardTile) item);

                    }
                }
            });
        });

        view.getManualSetupButton().setOnMouseClicked(mouseEvent -> {
            toggleManualSetupPhase();
            view.isManualSetupButtonVisible(manualSetupPhase);
        });
    }

    private void manualSetupMove(MouseEvent event, BoardTile item){
        //Select switch origin
        if (!switchOriginSelected) {
            switchOriginSelected = selectSwitchOrigin(item);
            event.consume();
            return;
        }

        //Select switch destination
        boolean switchDestinationSelected = selectSwitchDestination(item);
        if(!switchDestinationSelected) {
            switchOriginSelected = false; //User needs to make new move
            event.consume();
            return;
        }
        //Make the switch
        switchMove.switchPawn();
        updateBoardWithMove(switchMove);

        switchOriginSelected = false;
    }

    private void playerMove(MouseEvent event, BoardTile item){
        //Select origin
        if (!originSelected) {
            originSelected = setMoveOrigin(item);
            if(originSelected){
                view.getBoardSection().setBorderAroundTile(move.getOriginTile().getRow(), move.getOriginTile().getCol(), Color.BLUE);
            }
            event.consume();
            return;
        }

        //Select destination
        boolean destinationSelected = setMoveDestination(item);
        if (!destinationSelected) {
            originSelected = false; //User needs to make new move
            view.getBoardSection().setNoBorderAroundTile(move.getOriginTile().getRow(), move.getOriginTile().getCol(), Color.BLUE);
            event.consume();
            return;
        }

        LandTile destination = move.getDestinationTile();

        boolean validMove = false;
        try {
            validMove = move.isValidMove();
        } catch (IllegalMoveException ime) {

        }

        if (validMove) {
            eventToHandle = Events.REVEAL;
        } else {
            view.getBoardSection().setNoBorderAroundTile(move.getOriginTile().getRow(), move.getOriginTile().getCol(), Color.BLUE);
            originSelected = false; //User needs to make new move
            event.consume();
            return;
        }
    }

    private void handleEvents(ActionEvent event){
        if(eventToHandle != Events.START){
            if(eventToHandle == Events.REVEAL){
                LandTile destination = move.getDestinationTile();
                int row = destination.getRow();
                int col = destination.getCol();

                revealTile(row, col);

                eventToHandle = Events.PLAYERMOVE;
                event.consume();
                return;
            }

            if(eventToHandle == Events.PLAYERMOVE){
                //Make the move
                Player computerPlayer = Model.getComputerPlayer();
                Player player = Model.getHumanPlayer();
                move.movePawn();
                updateBoardWithMove(move);

                if (model.didUserWinFrom(computerPlayer)) {
                    showWinnerAlert(Alert.AlertType.INFORMATION, player);
                    event.consume();
                    showNewGameWindow();
                }

                originSelected = false;
                eventToHandle = Events.COMPUTERMOVEREVEAL;
                event.consume();
                return;
            }

            if(eventToHandle == Events.COMPUTERMOVEREVEAL){
                //Computer move
                computerMove = model.getComputerPlayer().makeMove();

                Player player = Model.getHumanPlayer();

                LandTile originTile = computerMove.getOriginTile();
                int originRow = originTile.getRow();
                int originCol = originTile.getCol();

                LandTile destinationTile = computerMove.getDestinationTile();

                if (destinationTile.getPawn() != null) {
                    if (destinationTile.getPawn().getColor() == player.getColor()){
                        revealTile(originRow, originCol);
                    }
                }

                int row = computerMove.getOriginTile().getRow();
                int col = computerMove.getOriginTile().getCol();
                view.getBoardSection().setBorderAroundTile(row, col, Color.RED);

                eventToHandle = Events.WAIT;
                event.consume();
                return;
            }

            //HACK om de beweging van de computer uit te stellen met 600 millis en bijgevolg duidelijker te maken.
            if(eventToHandle == Events.WAIT){
                eventToHandle = Events.COMPUTERMOVERESULT;
                event.consume();
                return;
            }

            if(eventToHandle == Events.COMPUTERMOVERESULT){
                Player computerPlayer = Model.getComputerPlayer();
                Player player = Model.getHumanPlayer();
                computerMove.movePawn();

                updateBoardWithMove(computerMove);

                if (model.didUserWinFrom(player)) {
                    showWinnerAlert(Alert.AlertType.INFORMATION, computerPlayer);
                    event.consume();
                    showNewGameWindow();
                }

                eventToHandle = Events.START;
                event.consume();
                return;
            }

        }
    }

    private boolean selectSwitchOrigin(Node item){
        Player player = model.getHumanPlayer();
        switchMove = new Move(player.getColor());

        BoardTile origin = (BoardTile) item;
        int[] originCoordinates = origin.getCoordinates();

        try {
            switchMove.setOriginTile(originCoordinates[0], originCoordinates[1]);
        } catch (Exception e) {
            return false; //Go out of function
        }
        return true;
    }

    private boolean selectSwitchDestination(Node item){
        Player player = model.getHumanPlayer();
        MoveHistory humanMoveHistory = player.getMoveHistory();

        BoardTile destination = (BoardTile) item;
        int[] destinationCoordinates = destination.getCoordinates();

        try {
            switchMove.setSwitchDestinationTile(destinationCoordinates[0], destinationCoordinates[1]);
        } catch (Exception e) {
            //If wrong destination choice: stop event
            return false; //Go out of function
        }

        return true;
    }

    private void showWinnerAlert(Alert.AlertType alertType, Player player){
        WinnerAlert winnerAlert = new WinnerAlert(alertType, player);
        winnerAlert.showAndWait();
    }

    private boolean setMoveOrigin(Node item){
        Player player = model.getHumanPlayer();
        move = new Move(player.getColor());

        BoardTile origin = (BoardTile) item;
        int[] originCoordinates = origin.getCoordinates();

        try {
            move.setOriginTile(originCoordinates[0], originCoordinates[1]);
        } catch (Exception e) {
            return false; //Go out of function
        }
        return true;
    }

    private boolean setMoveDestination(Node item){
        Player player = model.getHumanPlayer();
        MoveHistory humanMoveHistory = player.getMoveHistory();

        BoardTile destination = (BoardTile) item;
        int[] destinationCoordinates = destination.getCoordinates();

        try {
            move.setDestinationTile(destinationCoordinates[0], destinationCoordinates[1]);
        } catch (Exception e) {
            //If wrong destination choice: stop event
            return false; //Go out of function
        }

        if (humanMoveHistory.moveRepeatedTooOften(move)) {
            TooMuchSameMovesAlert tooMuchSameMovesAlert = new TooMuchSameMovesAlert();
            tooMuchSameMovesAlert.showAndWait();
            return false; //Go out of function
        }
        return true;
    }

    public void showNewGameWindow(){
        NewGameScreenView newGameScreenView = new NewGameScreenView(uiSettings);
        NewGameScreenPresenter newGameScreenPresenter = new NewGameScreenPresenter(model, newGameScreenView, uiSettings);
        Stage newGameScreenStage = new Stage();
        newGameScreenStage.initOwner(view.getScene().getWindow());
        newGameScreenStage.initModality(Modality.APPLICATION_MODAL);
        Scene scene = new Scene(newGameScreenView);
        newGameScreenStage.setScene(scene);

        double height = uiSettings.getResY() / 8;
        double width = uiSettings.getResX() / 3.5;
        Window window = view.getScene().getWindow();
        newGameScreenStage.setX(window.getX() + (view.getScene().getWidth()-width)/2);
        newGameScreenStage.setY(window.getY() + (view.getScene().getHeight()-height)/2);

        if (uiSettings.styleSheetAvailable()) {
            newGameScreenView.getScene().getStylesheets().removeAll();
            try {
                newGameScreenView.getScene().getStylesheets().add(uiSettings.getStyleSheetPath().toUri().toURL().toString());
            } catch (MalformedURLException ex) {
                // do nothing, if toURL-conversion fails, program can continue
            }
        }
        newGameScreenStage.initStyle(StageStyle.UNDECORATED); //No close, minimize, maximize button
        newGameScreenStage.showAndWait(); //showAndWait geeft error.

        if(gameStarted && !newGameCancelled) {
            initializeBoardSection();
            view.isManualSetupButtonVisible(true);
        }
    }

    private void handleAboutEvent(){
        AboutScreenView aboutScreenView = new AboutScreenView(uiSettings);
        AboutScreenPresenter aboutScreenPresenter = new AboutScreenPresenter(model, aboutScreenView, uiSettings);
        Stage aboutScreenStage = new Stage();
        aboutScreenStage.initOwner(view.getScene().getWindow());
        aboutScreenStage.initModality(Modality.APPLICATION_MODAL);
        Scene scene = new Scene(aboutScreenView);
        aboutScreenStage.setScene(scene);
        double height = uiSettings.getResY() / 2;
        double width = uiSettings.getResX() / 4;
        Window window = view.getScene().getWindow();
        aboutScreenStage.setX(window.getX() + (view.getScene().getWidth()-width)/2);
        aboutScreenStage.setY(window.getY() + (view.getScene().getHeight()-height)/2);
        aboutScreenView.getScene().getWindow().setHeight(height);
        aboutScreenView.getScene().getWindow().setWidth(width);
        if (uiSettings.styleSheetAvailable()) {
            aboutScreenView.getScene().getStylesheets().removeAll();
            try {
                aboutScreenView.getScene().getStylesheets().add(uiSettings.getStyleSheetPath().toUri().toURL().toString());
            } catch (MalformedURLException ex) {
                // do nothing, if toURL-conversion fails, program can continue
            }
        }
        aboutScreenStage.initStyle(StageStyle.UNDECORATED); //No close, minimize, maximize button
        aboutScreenStage.showAndWait();
    }

    private void handleInfoEvent(){
        InfoScreenView infoScreenView = new InfoScreenView(uiSettings);
        InfoScreenPresenter infoScreenPresenter = new InfoScreenPresenter(model, infoScreenView, uiSettings);
        Stage infoScreenStage = new Stage();
        infoScreenStage.initOwner(view.getScene().getWindow());
        infoScreenStage.initModality(Modality.APPLICATION_MODAL);
        Scene scene = new Scene(infoScreenView);
        infoScreenStage.setScene(scene);
        infoScreenStage.setX(view.getScene().getWindow().getX() + 10);
        infoScreenStage.setY(view.getScene().getWindow().getY() + 50);
        //infoScreenView.getScene().getWindow().setHeight(uiSettings.getResY()/2);
        infoScreenView.getScene().getWindow().setWidth(uiSettings.getResX()/2);
        if (uiSettings.styleSheetAvailable()){
            infoScreenView.getScene().getStylesheets().removeAll();
            try {
                infoScreenView.getScene().getStylesheets().add(uiSettings.getStyleSheetPath().toUri().toURL().toString());
            }
            catch (MalformedURLException ex) {
                // do nothing, if toURL-conversion fails, program can continue
            }
        }
        infoScreenStage.initStyle(StageStyle.UNDECORATED); //No close, minimize, maximize button
        infoScreenStage.showAndWait();
    }

    public void windowsHandler() {
        view.getScene().getWindow().setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) { handleCloseEvent(event); }});
    }

    private void handleCloseEvent(Event event){
        final Alert stopWindow = new Alert(Alert.AlertType.CONFIRMATION);
        stopWindow.setHeaderText("You're closing the application.");
        stopWindow.setContentText("Are you sure? Unsaved data may be lost.");
        stopWindow.setTitle("WARNING!");
        stopWindow.getButtonTypes().clear();
        ButtonType noButton = new ButtonType("No");
        ButtonType yesButton = new ButtonType("Yes");
        stopWindow.getButtonTypes().addAll(yesButton, noButton);
        stopWindow.showAndWait();
        if (stopWindow.getResult() == null || stopWindow.getResult().equals(noButton)) {
            event.consume();
        } else {
            view.getScene().getWindow().hide();
        }
    }

    private void updateLifeYards(){
        Army computerPlayerArmy = model.getComputerPlayer().getArmy();
        PlayerSection computerPlayerSection = view.getComputerPlayerSection();
        for (int rank = 1; rank <= 10; rank++){
            int nbPawnsWithRank = computerPlayerArmy.getNumberAlivePawnsWithRank(rank);
            computerPlayerSection.setAmount(true, rank-1, nbPawnsWithRank);
        }

        Army humanPlayerArmy = model.getHumanPlayer().getArmy();
        PlayerSection playerSection = view.getPlayerSection();
        for (int rank = 1; rank <= 10; rank++){
            int nbPawnsWithRank = humanPlayerArmy.getNumberAlivePawnsWithRank(rank);
            playerSection.setAmount(false, rank-1, nbPawnsWithRank);
        }
    }

    private void updateBoardWithMove(Move move){
        updateTile(move.getOriginTile().getRow(), move.getOriginTile().getCol());
        updateTile(move.getDestinationTile().getRow(), move.getDestinationTile().getCol());
        updateLifeYards();
        updateScore();
    }

    private void updateScore(){
        view.setComputerScore(model.getComputerPlayer().getScore());
        view.setPlayerScore(model.getHumanPlayer().getScore());
    }

    public void updateTile(int row, int col){
        BoardSection boardSection = view.getBoardSection();
        Player computerPlayer = model.getComputerPlayer();
        Tile tile = null;
        boolean computerPawn = false;

        try {
            tile = Board.getInstance().getTile(row,col);
        } catch (IllegalTileSelection illegalTileSelection) {
            illegalTileSelection.printStackTrace();
        }

        if( tile instanceof WaterTile) {
            boardSection.setNoImage(row, col);
            return; //Go out of the function
        }

        LandTile landTile = (LandTile) tile;
        Pawn pawn = landTile.getPawn();
        if(pawn == null){
            boardSection.setNoImage(row, col);
            boardSection.setColor(row, col, null);
            return; //Go out of the function
        }

        Color pawnColor = pawn.getColor();
        boardSection.setColor(row, col, pawnColor);

        if(pawn.getColor() == computerPlayer.getColor()){
            computerPawn = true;
        }

        if(computerPawn)
            boardSection.setMaskedImage(row, col);
        else{
            boardSection.setPawnImage(pawn.getRank(), row, col);
        }
    }

    public void revealTile(int row, int col){
        BoardSection boardSection = view.getBoardSection();
        Tile tile = null;

        try {
            tile = Board.getInstance().getTile(row,col);
        } catch (IllegalTileSelection illegalTileSelection) {
            illegalTileSelection.printStackTrace();
        }

        if( tile instanceof WaterTile) {
            boardSection.setNoImage(row, col);
            return; //Go out of the function
        }

        LandTile landTile = (LandTile) tile;
        Pawn pawn = landTile.getPawn();
        if(pawn == null){
            boardSection.setNoImage(row, col);
            boardSection.setColor(row, col, null);
            return; //Go out of the function
        }

        Color pawnColor = pawn.getColor();
        boardSection.setColor(row, col, pawnColor);

        boardSection.setPawnImage(pawn.getRank(), row, col);
    }

    public void initializeBoardSection() {
        BoardTile[][] board = view.getBoardSection().getBoard();

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                updateTile(i, j);
            }
        }

        updateLifeYards();
        updateScore();
    }

    private void updateView() {
    }

    public static void setGameStarted(boolean started){
        gameStarted = started;
    }

    public static void setNewGameCancelled(boolean cancelled){
        newGameCancelled = cancelled;
    }

    public static void toggleManualSetupPhase() {
        manualSetupPhase = !manualSetupPhase;
    }

    public static void setManualSetupPhase(boolean manualSetup){
        manualSetupPhase = manualSetup;
    }
}
