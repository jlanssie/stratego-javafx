package Stratego.View.AboutScreen;

import java.net.MalformedURLException;
import java.nio.file.Files;
import Stratego.View.UISettings;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

public class AboutScreenView extends VBox {

    private UISettings uiSettings;
    private Label versionLabel;
    private Label creatorsLabel;
    private ProgressBar timeProgressBar;
    private BorderPane progressPane;
    private ImageView logoImageView;
    private Image logoImage;
    private Button closeBtn;

    public AboutScreenView(UISettings uiSettings) {
        this.uiSettings = uiSettings;
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        versionLabel = new Label("Version 1.0");
        creatorsLabel = new Label("Created by Jeremy Lanssiers & Mante Bos");
        timeProgressBar = new ProgressBar();
        progressPane = new BorderPane();
        closeBtn = new Button("Close");
        try{
            logoImage = new Image(uiSettings.getStartScreenImagePath().toUri().toURL().toString());
        } catch (MalformedURLException e) {
            //Nothing to do, if image not found, nothing is shown
        }
    }

    private void layoutNodes() {
        int ImageSize = uiSettings.getLowestRes()/5;
        if (Files.exists(uiSettings.getStartScreenImagePath())) {
            logoImageView = new ImageView(logoImage);
            logoImageView.setPreserveRatio(true);
            logoImageView.setFitHeight(ImageSize);
            logoImageView.setFitWidth(ImageSize);
            logoImageView.setSmooth(true);
        }

        this.getChildren().addAll(logoImageView, creatorsLabel, versionLabel, closeBtn);
        this.setAlignment(Pos.CENTER);
        this.setPadding(new Insets(uiSettings.getInsetsMargin()));
        this.setSpacing(uiSettings.getInsetsMargin());
    }

    Button getCloseBtn() {
        return closeBtn;
    }
}
