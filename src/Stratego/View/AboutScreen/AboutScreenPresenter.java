package Stratego.View.AboutScreen;

import Stratego.Model.*;
import Stratego.View.UISettings;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class AboutScreenPresenter {

    private Model model;
    private AboutScreenView view;
    private UISettings uiSettings;

    public AboutScreenPresenter(Model model, AboutScreenView view, UISettings uiSettings) {
        this.model = model;
        this.view = view;
        this.uiSettings = uiSettings;
        view.getCloseBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                view.getScene().getWindow().hide();
            }
        });
    }
}
