package Stratego.View.InfoScreen;

import Stratego.Model.Army;
import Stratego.View.UISettings;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

import java.net.MalformedURLException;


public class InfoScreenView extends VBox {

    private UISettings uiSettings;
    private Label titleLbl;
    private TextArea InfoText;
    private Button okButton;
    private GridPane pawnsOverviewLeft;
    private GridPane pawnsOverviewRight;
    private ImageView flagImgView;
    private Image flagImg;
    private ImageView bombImgView;
    private Image bombImg;
    private ImageView spyImgView;
    private Image spyImg;
    private ImageView scoutImgView;
    private Image scoutImg;
    private ImageView minerImgView;
    private Image minerImg;
    private ImageView sergeantImgView;
    private Image sergeantImg;
    private ImageView lieutenantImgView;
    private Image lieutenantImg;
    private ImageView captainImgView;
    private Image captainImg;
    private ImageView majorImgView;
    private Image majorImg;
    private ImageView colonelImgView;
    private Image colonelImg;
    private ImageView generalImgView;
    private Image generalImg;
    private ImageView marshallImgView;
    private Image marshallImg;


    public InfoScreenView(UISettings uiSettings) {
        this.uiSettings = uiSettings;
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        InfoText = new TextArea("test");
        okButton = new Button("Close");
        titleLbl = new Label("The rules of Stratego");
        pawnsOverviewLeft = new GridPane();
        pawnsOverviewRight = new GridPane();

        try{
            flagImg = new Image(uiSettings.getPawnImagePath(11).toUri().toURL().toString());
            bombImg = new Image(uiSettings.getPawnImagePath(12).toUri().toURL().toString());
            spyImg = new Image(uiSettings.getPawnImagePath(10).toUri().toURL().toString());
            scoutImg = new Image(uiSettings.getPawnImagePath(9).toUri().toURL().toString());
            minerImg = new Image(uiSettings.getPawnImagePath(8).toUri().toURL().toString());
            sergeantImg = new Image(uiSettings.getPawnImagePath(7).toUri().toURL().toString());
            lieutenantImg = new Image(uiSettings.getPawnImagePath(6).toUri().toURL().toString());
            captainImg = new Image(uiSettings.getPawnImagePath(5).toUri().toURL().toString());
            majorImg = new Image(uiSettings.getPawnImagePath(4).toUri().toURL().toString());
            colonelImg = new Image(uiSettings.getPawnImagePath(3).toUri().toURL().toString());
            generalImg = new Image(uiSettings.getPawnImagePath(2).toUri().toURL().toString());
            marshallImg = new Image(uiSettings.getPawnImagePath(1).toUri().toURL().toString());
        } catch (MalformedURLException e) {
            //Nothing to do, if image not found, nothing is shown
        }
    }

    private void layoutNodes() {

        titleLbl.setFont(UISettings.font(20, FontWeight.BOLD));
        HBox titleBox = new HBox(titleLbl);
        titleBox.setAlignment(Pos.CENTER_LEFT);
        titleBox.setPadding(new Insets(0,0,0,5));

        VBox rulesVBox = new VBox();
        InfoText.setWrapText(true);
        InfoText.setEditable(false);
        InfoText.setPrefRowCount(22);
        InfoText.setStyle("-fx-focus-color: transparent ; -fx-faint-focus-color: transparent; -fx-background-color:transparent "); //removes blue focus border

        flagImgView = getPawnImageView(flagImg);
        bombImgView = getPawnImageView(bombImg);
        spyImgView = getPawnImageView(spyImg);
        scoutImgView = getPawnImageView(scoutImg);
        minerImgView = getPawnImageView(minerImg);
        sergeantImgView = getPawnImageView(sergeantImg);
        lieutenantImgView = getPawnImageView(lieutenantImg);
        captainImgView = getPawnImageView(captainImg);
        majorImgView = getPawnImageView(majorImg);
        colonelImgView = getPawnImageView(colonelImg);
        generalImgView = getPawnImageView(colonelImg);
        marshallImgView = getPawnImageView(marshallImg);

        //Pawn overview
        addHeaderToPawnOverview(pawnsOverviewLeft);
        addPawnToOverview(pawnsOverviewLeft,1, marshallImgView, Army.PAWN_TYPES.MAARSCHALK.name().toLowerCase(), Army.PAWN_TYPES.MAARSCHALK.rank, Army.PAWN_TYPES.MAARSCHALK.numberOfPanws );
        addPawnToOverview(pawnsOverviewLeft, 2, generalImgView, Army.PAWN_TYPES.GENERAL.name().toLowerCase(), Army.PAWN_TYPES.GENERAL.rank, Army.PAWN_TYPES.GENERAL.numberOfPanws );
        addPawnToOverview(pawnsOverviewLeft,3, colonelImgView, Army.PAWN_TYPES.COLONEL.name().toLowerCase(), Army.PAWN_TYPES.COLONEL.rank, Army.PAWN_TYPES.COLONEL.numberOfPanws );
        addPawnToOverview(pawnsOverviewLeft,4, majorImgView, Army.PAWN_TYPES.MAJOR.name().toLowerCase(), Army.PAWN_TYPES.MAJOR.rank, Army.PAWN_TYPES.MAJOR.numberOfPanws );
        addPawnToOverview(pawnsOverviewLeft, 5, captainImgView, Army.PAWN_TYPES.CAPTAIN.name().toLowerCase(), Army.PAWN_TYPES.CAPTAIN.rank, Army.PAWN_TYPES.CAPTAIN.numberOfPanws );
        addPawnToOverview(pawnsOverviewLeft, 6, lieutenantImgView, Army.PAWN_TYPES.LIEUTENANT.name().toLowerCase(), Army.PAWN_TYPES.LIEUTENANT.rank, Army.PAWN_TYPES.LIEUTENANT.numberOfPanws );
        pawnsOverviewLeft.setHgap(15);
        pawnsOverviewLeft.setVgap(10);

        addHeaderToPawnOverview(pawnsOverviewRight);
        addPawnToOverview(pawnsOverviewRight,1, sergeantImgView, Army.PAWN_TYPES.SERGEANT.name().toLowerCase(), Army.PAWN_TYPES.SERGEANT.rank, Army.PAWN_TYPES.SERGEANT.numberOfPanws );
        addPawnToOverview(pawnsOverviewRight, 2, minerImgView, Army.PAWN_TYPES.MINER.name().toLowerCase(), Army.PAWN_TYPES.MINER.rank, Army.PAWN_TYPES.MINER.numberOfPanws );
        addPawnToOverview(pawnsOverviewRight,3, scoutImgView, Army.PAWN_TYPES.SCOUT.name().toLowerCase(), Army.PAWN_TYPES.SCOUT.rank, Army.PAWN_TYPES.SCOUT.numberOfPanws );
        addPawnToOverview(pawnsOverviewRight,4, spyImgView, Army.PAWN_TYPES.SPY.name().toLowerCase(), Army.PAWN_TYPES.SPY.rank, Army.PAWN_TYPES.SPY.numberOfPanws );
        addPawnToOverview(pawnsOverviewRight, 5, bombImgView, Army.PAWN_TYPES.BOMB.name().toLowerCase(), -1, Army.PAWN_TYPES.BOMB.numberOfPanws );
        addPawnToOverview(pawnsOverviewRight, 6, flagImgView, Army.PAWN_TYPES.FLAG.name().toLowerCase(), -1, Army.PAWN_TYPES.FLAG.numberOfPanws );
        pawnsOverviewRight.setHgap(15);
        pawnsOverviewRight.setVgap(10);


        BorderPane pawnsOverview = new BorderPane();
        pawnsOverview.setLeft(pawnsOverviewLeft);
        pawnsOverview.setRight(pawnsOverviewRight);
        pawnsOverview.setPadding(new Insets(0, uiSettings.getInsetsMargin(), 0, uiSettings.getInsetsMargin()));
        rulesVBox.getChildren().addAll(InfoText, pawnsOverview);

        this.getChildren().addAll(titleBox, rulesVBox, okButton);
        this.setAlignment(Pos.CENTER);
        this.setPadding(new Insets(uiSettings.getInsetsMargin()));
        /*this.setTop(titleLbl);
        BorderPane.setMargin(titleLbl, new Insets(uiSettings.getInsetsMargin()/6));
        this.setCenter(rulesVBox);
        BorderPane.setMargin(rulesVBox, new Insets(uiSettings.getInsetsMargin()));
        this.setBottom(okButton);
        BorderPane.setAlignment(okButton, Pos.CENTER);
        this.setPadding(new Insets(uiSettings.getInsetsMargin()));
        setPrefWidth(uiSettings.getLowestRes() / 4);*/
    }

    private void addHeaderToPawnOverview(GridPane pawnsOverview) {
        Font headerFont = UISettings.font(15, FontWeight.BOLD);
        Text pawnLbl = new Text("Pawn");
        pawnLbl.setFont(headerFont);
        Label rankLbl = new Label("Rank");
        rankLbl.setFont(headerFont);
        Label numberPieces = new Label("Number of pieces");
        numberPieces.setFont(headerFont);

        pawnsOverview.setConstraints(pawnLbl, 0, 0, 2, 1);
        pawnsOverview.add(pawnLbl, 0, 0);
        pawnsOverview.setHalignment(pawnLbl, HPos.CENTER);
        pawnsOverview.add(rankLbl, 2, 0);
        pawnsOverview.setHalignment(rankLbl, HPos.CENTER);
        pawnsOverview.add(numberPieces, 3,0);
        pawnsOverview.setHalignment(numberPieces, HPos.CENTER);
    }

    private void addPawnToOverview(GridPane pawnsOverview, int row, ImageView imageView, String name, int rank, int numberOfPieces){
        pawnsOverview.add(imageView, 0, row);
        pawnsOverview.add(new Label(name), 1, row);
        if(rank > 0) {
            Label rankLbl = new Label( Integer.toString(rank));
            pawnsOverview.add(rankLbl, 2, row);
            pawnsOverview.setHalignment(rankLbl, HPos.CENTER);
        }
        Label nbPieces = new Label("x" + numberOfPieces);
        pawnsOverview.add(nbPieces, 3, row);
        pawnsOverview.setHalignment(nbPieces, HPos.CENTER);
    }

    private ImageView getPawnImageView(Image image){
        ImageView imageView = new ImageView(image);
        imageView.setPreserveRatio(true);
        imageView.setFitHeight(40);
        imageView.setFitWidth(40);
        imageView.setSmooth(true);
        return imageView;
    }

    TextArea getInfoText () {return InfoText;}

    Button getBtnOk() {
        return okButton;
    }
}
