package Stratego.View.StartScreen;

import Stratego.View.UISettings;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;

import java.awt.*;
import java.net.MalformedURLException;
import java.nio.file.Files;

public class StartScreenView extends VBox  {

    private UISettings uiSettings;
    private Label timeDisplayLabel;
    private Label versionLabel;
    private Label creatorsLabel;
    private ProgressBar timeProgressBar;
    private BorderPane progressPane;
    private ImageView logoImageView;
    private Image logoImage;
    private StartScreenTransition trans;

    public StartScreenView(UISettings uiSettings) {
        this.uiSettings = uiSettings;
        initialiseNodes();
        layoutNodes();
        animate();
    }

    private void initialiseNodes() {
        timeDisplayLabel = new Label("Loading: 0.0");
        versionLabel = new Label("Version 1.0");
        creatorsLabel = new Label("Created by Jeremy Lanssiers & Mante Bos");
        timeProgressBar = new ProgressBar();
        progressPane = new BorderPane();
        try{
            logoImage = new Image(uiSettings.getStartScreenImagePath().toUri().toURL().toString());
        } catch (MalformedURLException e) {
            //Nothing to do, if image not found, nothing is shown
        }
    }

    private void layoutNodes() {
        int ImageSize = uiSettings.getLowestRes()/5;
        if (Files.exists(uiSettings.getStartScreenImagePath())) {
            logoImageView = new ImageView(logoImage);
            logoImageView.setPreserveRatio(true);
            logoImageView.setFitHeight(ImageSize);
            logoImageView.setFitWidth(ImageSize);
            logoImageView.setSmooth(true);
        }

        progressPane.setRight(this.timeProgressBar);
        progressPane.setLeft(this.timeDisplayLabel);
        BorderPane.setMargin(this.timeDisplayLabel, new Insets(uiSettings.getInsetsMargin()));
        BorderPane.setMargin(this.timeProgressBar, new Insets(uiSettings.getInsetsMargin()));

        VBox vbox = new VBox(creatorsLabel, versionLabel);
        vbox.setAlignment(Pos.CENTER);

        this.getChildren().addAll(logoImageView, vbox, progressPane);
        this.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        this.setAlignment(Pos.CENTER);
        //this.setSpacing(uiSettings.getInsetsMargin());
    }

    Label getTimeDisplayLabel() {return (timeDisplayLabel);}

    ProgressBar getTimeProgressBar() {return (timeProgressBar);}

    StartScreenTransition getTransition() {return trans;}

    private void animate() {
        trans = new StartScreenTransition(this,5);
        trans.play();
    }

}
