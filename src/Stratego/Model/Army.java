package Stratego.Model;

import java.util.ArrayList;

public class Army {
    private ArrayList<Pawn> army = new ArrayList<>();
    private ArrayList<Pawn> killedPawns = new ArrayList<>();
    public final static int NUMBER_PAWNS = 40;
    private final Color color;

    public enum PAWN_TYPES {
        MAARSCHALK(1, 1,1),
        GENERAL(2, 1, 1),
        COLONEL(3,1, 2),
        MAJOR(4, 1,3),
        CAPTAIN(5, 1,4),
        LIEUTENANT(6, 1,4),
        SERGEANT(7,1,4),
        MINER(8,1,5),
        SCOUT(9, Board.NUMBER_ROWS,8),
        SPY(10, 1,1),
        FLAG(11,0, 1),
        BOMB(12, 0,6);

        public int rank;
        public int numberOfPanws;
        int maxSteps;

        PAWN_TYPES(int rank, int maxSteps, int numberOfPanws) {
            this.rank = rank;
            this.numberOfPanws = numberOfPanws;
            this.maxSteps = maxSteps;
        }
    }

    public Army(Color color) {
        //Fill up the arraylist, to create the army
        for( PAWN_TYPES pawn_type : PAWN_TYPES.values()) {
            for(int i = 0; i < pawn_type.numberOfPanws; i++) {
                army.add(new Pawn(pawn_type.name(), pawn_type.rank, pawn_type.maxSteps, color));
            }
        }
        this.color = color;
    }

    public ArrayList<Pawn> getArmy() {
        return army;
    }

    public boolean hasPawn(Pawn pawn) {
        return army.contains(pawn);
    }

    public void killPawn(Pawn pawn) {
        killedPawns.add(pawn);
        army.remove(pawn);
    }

    public ArrayList<Pawn> cloneArmy() {
        ArrayList<Pawn> clone = new ArrayList<>();
        clone.addAll(army);
        return clone;
    }

    public boolean canStillMove() {
        if(!hasMoveablePawns())
            return false;
        return hasPawnsThatCanMoveOnBoard();
    }

    private boolean hasMoveablePawns(){
        for(Pawn p : army) {
            if(p.isMovable()) {
                return true;
            }
        }
        return false;
    }

    public int getNumberAlivePawnsWithRank(int rank){
        int sum = 0;
        for (Pawn pawn : army) {
            if(pawn.getRank() == rank)
                sum++;
        }
        return sum;
    }

    private boolean hasPawnsThatCanMoveOnBoard(){
        Board board = Board.getInstance();
        ArrayList<LandTile>  tiles = board.getTilesWithPawnsOfColor(color);
        Move move = new Move(color);
        for(LandTile tile : tiles){
            if(move.canMoveDownFrom(tile) || move.canMoveLeftFrom(tile) ||
            move.canMoveRightFrom(tile) || move.canMoveUpFrom(tile))
                return true;
        }
        return false;
    }

    public boolean isFlagCaptured() {
        for(Pawn p: killedPawns) {
            if(p.getRank() == PAWN_TYPES.FLAG.rank) {
                return true;
            }
        }
        return false;
    }
}
