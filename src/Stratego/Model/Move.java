package Stratego.Model;

import Stratego.Model.Exceptions.*;
import java.util.Scanner;

public class Move {

    private Color playerColor;
    private LandTile originTile;
    private LandTile destinationTile;
    private static final Scanner keyboard = new Scanner(System.in);
    private static Board board = Board.getInstance();

    public Move(Color playerColor){
        this.playerColor = playerColor;
    }

    public LandTile getOriginTile() {
        return originTile;
    }

    public LandTile getDestinationTile() {
        return destinationTile;
    }

    public void setOriginTile(int row, int col) throws IllegalMoveException{
        Tile tile = null;
        try {
            tile = board.getTile(row, col);
        }catch (IllegalTileSelection e){
            //Do nothing -> will be thrown in next if
        }

        if(isValidOriginTile(tile))
            originTile = (LandTile) tile;
        else
            throw new IllegalMoveException("Invalid origin tile");
    }

    public void setOriginTile(Tile tile) throws IllegalMoveException{
        setOriginTile(tile.getRow(), tile.getCol());
    }

    public void setDestinationTile(int row, int col) throws IllegalMoveException{
        Tile tile = null;
        try {
            tile = board.getTile(row, col);
        }catch (IllegalTileSelection e){
            //Do nothing -> will be thrown in next if
        }

        if(isValidDestinationTile(tile))
            destinationTile = (LandTile) tile;
        else
            throw new IllegalMoveException("Invalid destination tile");
    }

    public void setSwitchDestinationTile(int row, int col) throws IllegalMoveException{
        Tile tile = null;
        try {
            tile = board.getTile(row, col);
        }catch (IllegalTileSelection e){
            //Do nothing -> will be thrown in next if
        }

        if(isValidSwitchDestinationTile(tile))
            destinationTile = (LandTile) tile;
        else
            throw new IllegalMoveException("Invalid destination tile");
    }

    public void setDestinationTile(Tile tile)throws IllegalMoveException{
        setDestinationTile(tile.getRow(), tile.getCol());
    }

    public boolean canMoveUpFrom(Tile tile){
        Tile toTile = null;
        int row = tile.getRow()-1;
        int col = tile.getCol();
        try {
            toTile = board.getTile(row, col);
        } catch (IllegalTileSelection e) {
            return false;
        }
        return canMoveFromTo(tile, toTile);
    }

    public boolean canMoveDownFrom(Tile tile){
        Tile toTile = null;
        int row = tile.getRow()+1;
        int col = tile.getCol();
        try {
            toTile = board.getTile(row, col);
        } catch (IllegalTileSelection e) {
            return false;
        }
        return canMoveFromTo(tile, toTile);
    }

    public boolean canMoveLeftFrom(Tile tile){
        Tile toTile = null;
        int row = tile.getRow();
        int col = tile.getCol()-1;
        try {
            toTile = board.getTile(row, col);
        } catch (IllegalTileSelection e) {
            return false;
        }
        return canMoveFromTo(tile, toTile);
    }

    public boolean canMoveRightFrom(Tile tile){
        Tile toTile = null;
        int row = tile.getRow();
        int col = tile.getCol()+1;
        try {
            toTile = board.getTile(row, col);
        } catch (IllegalTileSelection e) {
            return false;
        }
        return canMoveFromTo(tile, toTile);
    }

    private boolean canMoveFromTo(Tile fromTile, Tile toTile){
        try {
            setOriginTile(fromTile);
            setDestinationTile(toTile);
            return isValidMove();
        }catch (Exception e){
            return false;
        }
    }

    private boolean isValidOriginTile(Tile originTile){
        if(originTile == null)
            return false;

        if( !(originTile instanceof LandTile))
            return false;

        Pawn pawnToMove = ((LandTile)originTile).getPawn();
        if (pawnToMove == null)
            return false;

        if (pawnToMove.getColor() != playerColor)
            return false;

        return true;
    }

    private boolean isValidDestinationTile(Tile destinationTile){
        if(destinationTile == null)
            return false;

        if( !(destinationTile instanceof LandTile))
            return false;

        if(destinationTile == originTile)
            return false;

        Pawn pawnOnDestinationTile = ((LandTile) destinationTile).getPawn();
        if (pawnOnDestinationTile != null && pawnOnDestinationTile.getColor() == playerColor)
            return false;

        return true;
    }

    private boolean isValidSwitchDestinationTile(Tile destinationTile){
        if(destinationTile == null)
            return false;

        if( !(destinationTile instanceof LandTile))
            return false;

        if(destinationTile == originTile)
            return false;

        Pawn pawnOnDestinationTile = ((LandTile) destinationTile).getPawn();
        if (pawnOnDestinationTile != null && pawnOnDestinationTile.getColor() != playerColor)
            return false;

        return true;
    }

    public boolean isValidMove() throws IllegalMoveException {
        Pawn pawnToMove = originTile.getPawn();

        if( ! movesInStraightLine(originTile, destinationTile) )
            return false;

        int numberSteps = getNumberOfStepsBetweenTiles(originTile, destinationTile);
        if( ! pawnToMove.canMoveNumberOfSteps(numberSteps))
            return false;

        if(numberSteps > 1) {
            return pathIsValidBetweenTiles(originTile, destinationTile);
        }

        return true;
    }

    /*
        returns true if the tiles between originTile and destinationTile
            - only contains landTiles
            - only contains empty tiles (without pawns
        returns false otherwise
     */
    private boolean pathIsValidBetweenTiles(Tile originTile, Tile destinationTile){
        Board board = Board.getInstance();

        boolean isHorizontalMove = isHorizontalMove(originTile, destinationTile);

        int start = getStartIndexOfPath(originTile, destinationTile);
        int end = getEndIndexOfPath(originTile, destinationTile);
        int staticNumber = isHorizontalMove ? destinationTile.getRow() : destinationTile.getCol();
        for (int i = start + 1; i < end; i++) {

            Tile inBetweenTile = null;
            try {
                inBetweenTile =isHorizontalMove ? board.getTile(staticNumber, i) : board.getTile(i, staticNumber);
            } catch (IllegalTileSelection illegalTileSelection) {
                //Should not happen -> always legal path between legal originTile and legal destinationTile
                return false;
            }
            if( inBetweenTile instanceof WaterTile)
                return false;
            if( ((LandTile) inBetweenTile).getPawn() != null )
                return false;
        }

        return true;
    }

    private int getStartIndexOfPath(Tile originTile, Tile destinationTile){
        if(isHorizontalMove(originTile, destinationTile))
            return Math.min(originTile.getCol(), destinationTile.getCol());
        else
            return Math.min(originTile.getRow(), destinationTile.getRow());
    }

    private int getEndIndexOfPath(Tile originTile, Tile destinationTile){
        if(isHorizontalMove(originTile, destinationTile))
            return Math.max(originTile.getCol(), destinationTile.getCol());
        else
            return Math.max(originTile.getRow(), destinationTile.getRow());
    }

    private boolean isHorizontalMove(Tile originTile, Tile destinationTile){
        int nbHorizontalSteps = getNumberHorizontalSteps(originTile, destinationTile);
        int nbVerticalSteps = getNumberVerticalSteps(originTile, destinationTile);
        return nbHorizontalSteps != 0 && nbVerticalSteps == 0;
    }

    private boolean isVerticalMove(Tile originTile, Tile destinationTile){
        int nbHorizontalSteps = getNumberHorizontalSteps(originTile, destinationTile);
        int nbVerticalSteps = getNumberVerticalSteps(originTile, destinationTile);
        return nbHorizontalSteps == 0 && nbVerticalSteps != 0;
    }

    private int getNumberOfStepsBetweenTiles(Tile originTile, Tile destinationTile){
        int nbStepsRow = getNumberVerticalSteps(originTile, destinationTile);
        int nbStepsCol = getNumberHorizontalSteps(originTile, destinationTile);
        return nbStepsRow + nbStepsCol;
    }

    private boolean movesInStraightLine(Tile originTile, Tile destinationTile){
        boolean isVerticalMove = isVerticalMove(originTile, destinationTile);
        boolean isHorizontalMove = isHorizontalMove(originTile, destinationTile);
        return (isVerticalMove ^ isHorizontalMove); // ^ is an exclusive or -> only first or only second comparison can be true
    }

    private int getNumberVerticalSteps(Tile originTile, Tile destinationTile){
        return Math.abs(originTile.getRow() - destinationTile.getRow());
    }

    private int getNumberHorizontalSteps(Tile originTile, Tile destinationTile){
        return Math.abs(originTile.getCol() - destinationTile.getCol());
    }

    public void movePawn(){
        try {
            if(originTile != null && destinationTile != null && isValidMove())
                movePawn(originTile, destinationTile);
        } catch (IllegalMoveException e) {
            e.printStackTrace();
            //should not occur, function should not be called before it is checked if it is a valid move
        }
    }

    private void movePawn(LandTile originTile, LandTile destinationTile){
        Pawn attacker = originTile.getPawn();
        Player ownerAttacker = Model.getPlayerWithColor(attacker.getColor());

        Pawn defender = destinationTile.getPawn();
        if( defender == null){
            //destinationTile is empty -> just move the pawn from origin to destination
            destinationTile.setPawn(originTile.getPawn());
            originTile.removePawn();
        }
        else {
            Player ownerDefender =  Model.getPlayerWithColor(defender.getColor());

            int result = attacker.compareTo(defender);
            if (result > 0) {
                //Attacker wins, defender is dead
                killPawnDuringBattle(ownerDefender, destinationTile, defender);
                destinationTile.setPawn(originTile.getPawn());
                originTile.removePawn();
                ownerAttacker.increaseScore();
            } else if (result < 0) {
                //Attacker loses and dies
                killPawnDuringBattle(ownerAttacker, originTile, attacker);
                System.out.printf("against pawn %s with rank %d (of player %s%n", defender.getName(), defender.getRank(), Model.getPlayerWithColor(defender.getColor()));
                ownerDefender.increaseScore();
            } else {
                //Same rank -> both lose
                killPawnDuringBattle(ownerDefender, destinationTile, defender);
                killPawnDuringBattle(ownerAttacker, originTile, attacker);
                ownerAttacker.increaseScore();
                ownerDefender.increaseScore();
            }
        }
    }

    public void switchPawn(){
        if(originTile != null && destinationTile != null) {
            switchPawn(originTile, destinationTile);
        }
    }

    private void switchPawn(LandTile originTile, LandTile destinationTile){
        Pawn origin = originTile.getPawn();
        Player ownerOrigin = Model.getPlayerWithColor(origin.getColor());

        Pawn destination = destinationTile.getPawn();
        if (destination != null) {
            Player ownerDestination = Model.getPlayerWithColor(destination.getColor());

            if (ownerOrigin == ownerDestination) {
                originTile.setPawn(destination);
                destinationTile.setPawn(origin);
            }
        }
    }

    private void killPawnDuringBattle(Player player, LandTile tile, Pawn pawn){
        System.out.printf("%s lost pawn %s with rank %d on tile %d,%d%n", player.getName(), pawn.getName(), pawn.getRank(), tile.getRow(), tile.getCol());
        tile.removePawn();
        player.army.killPawn(pawn);
    }
}
