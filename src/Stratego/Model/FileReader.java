package Stratego.Model;

import Stratego.Model.Exceptions.CorruptedFileException;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class FileReader {
    private String filePath;
    private File file;

    public FileReader(String filePath) { file = new File(filePath); }

    public FileReader(File file){ this.file = file; }


    public ArrayList<int[]> readFile() throws FileNotFoundException, CorruptedFileException {
        Scanner scanner = new Scanner(file);
        ArrayList<int[]> rows = new ArrayList<>();
        while(scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] numbersText = line.split(":");
            try {
                int row = Integer.parseInt(numbersText[0]);
                int col = Integer.parseInt(numbersText[1]);
                int rank = Integer.parseInt(numbersText[2]);
                rows.add(new int[]{row, col, rank});
            } catch (NumberFormatException e) {
                throw new CorruptedFileException("The file contains incorrect characters");
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new CorruptedFileException("The file contains one or more rows with too little arguments");
            }
        }
        return rows;
    }
}
