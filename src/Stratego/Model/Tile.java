package Stratego.Model;

public abstract class Tile {
    private int row;
    private int col;

    Tile(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public int getRow() { return row; };
    public int getCol() { return col; }

    public abstract boolean isAccessible();
    public abstract boolean isEmtpy();

    @Override
    public abstract String toString();
}
