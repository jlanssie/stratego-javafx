package Stratego.Model;

public class LandTile extends Tile {
    private Pawn pawn;

    public LandTile(){
        //This makes a placeholder object in MainScreenPresenter to receive tile and pawn info
        super(0,0);
    };

    public LandTile(int row, int col) {
        super(row, col);
    }

    public Pawn getPawn() {
        return pawn;
    }

    public void setPawn(Pawn pawn) {
        this.pawn = pawn;
    }

    public void removePawn() { pawn = null; }

    public boolean isEmtpy() { return pawn == null; }

    @Override
    public boolean isAccessible() {
        return true; //Any tile can be accessed, except water tiles
    }

    @Override
    public String toString() {
        if(pawn == null)
            return "    ";
        if(Model.getPlayerWithColor(pawn.getColor()) instanceof ComputerPlayer)
            return "  X ";
        else
            return String.format(" %2d ", pawn.getRank());
    }
}