package Stratego.Model;

/**
 * @author Mante Bos
 * @version 1.0 01/03/2020 16:37 *
 */
public enum PawnPlacement {
    /*MANUAL,*/
    SMARTRANDOM,
    RANDOM,
    LOADSETUP;


    public static PawnPlacement getPlacementType(String type){
        type = type.toUpperCase();
        switch (type){
            /* case "MANUAL": return MANUAL; */
            case "SMART RANDOM": return SMARTRANDOM;
            case "RANDOM": return RANDOM;
            case "LOAD SETUP": return LOADSETUP;
            default: return null;
        }
    }
}
