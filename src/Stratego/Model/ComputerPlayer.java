package Stratego.Model;

import Stratego.Model.Exceptions.CorruptedFileException;
import Stratego.Model.Exceptions.IllegalMoveException;
import Stratego.Model.Exceptions.IllegalTileSelection;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class ComputerPlayer extends Player {

    private Random random = new Random();

    public ComputerPlayer(Color color, PlaceOnBoard place) {
        super("Computer", color, place);
    }

    @Override
    public void placePawns(){
        // 3 options: loadPreset, random or smartRandom
        double randomNumber = random.nextDouble();
        PawnSetup pawnSetup = new PawnSetup(army, place);

        if(randomNumber < 0.65)
            pawnSetup.createSmartRandomSetup();
        else if(randomNumber < 0.9){
            loadRandomPreset(pawnSetup);
        }
        else // 0.1 chance
            pawnSetup.createRandomSetup();
    }

    private void loadRandomPreset(PawnSetup pawnSetup){
        String[] filepaths =pawnSetup.getPresetFilepaths();
        int randomFilepath = random.nextInt(filepaths.length);
        try {
            pawnSetup.loadSetupFromFile(filepaths[randomFilepath]);
        } catch (FileNotFoundException | CorruptedFileException e) {
            e.printStackTrace();
            //Should not occur
        }
    }

    @Override
    public Move makeMove(){
        int[][] heuristics = getMoveHeuristics();
        LandTile originTile = selectTileToMoveFrom(heuristics);
        LandTile destinationTile = selectTileToMoveTo(originTile);
        Move move = new Move(getColor());
        try {
            move.setOriginTile(originTile);
            move.setDestinationTile(destinationTile);
        } catch (IllegalMoveException e) {
            e.printStackTrace(); //Should never occur
        }
        return move;
    }

    private LandTile selectTileToMoveTo(LandTile originTile){
        LandTile destinationTile = null;
        ArrayList<Direction> directions = getDirectionsToMoveTo(originTile);
        Collections.shuffle(directions);
        Direction directionToMove = directions.get(0);

        int row = originTile.getRow();
        int col = originTile.getCol();
        if(directionToMove == Direction.UP)
            row--;
        else if( directionToMove == Direction.DOWN)
            row++;
        else if( directionToMove == Direction.LEFT)
            col--;
        else
            col++;

        try {
            destinationTile = (LandTile) Board.getInstance().getTile(row, col);
        } catch (IllegalTileSelection illegalTileSelection) {
            //Cannot occur
        }

        return destinationTile;
    }

    private ArrayList<Direction> getDirectionsToMoveTo(LandTile originTile){
        ArrayList<Direction> directions = new ArrayList<>();
        Move move = new Move(getColor());

        if( move.canMoveUpFrom(originTile) ){
            directions.add(Direction.UP);
            if(!wantAggressivePlay()){
                directions.add(Direction.UP);
            }
        }

        if( move.canMoveDownFrom(originTile) ){
            directions.add(Direction.DOWN);
            if(wantAggressivePlay()){
                directions.add(Direction.DOWN);
            }
        }

        if( move.canMoveLeftFrom(originTile) ){
            directions.add(Direction.LEFT);
        }

        if( move.canMoveRightFrom(originTile) ){
            directions.add(Direction.RIGHT);
        }
        return directions;
    }

    private LandTile selectTileToMoveFrom(int[][] heuristics){
        //Take the top 5 best pawns to move and choose randomly 1
        ArrayList<LandTile> topTiles = new ArrayList<>();
        int counter = 0;
        while(counter < 5){
            LandTile tile = getTileWithMaxHeuristic(topTiles, heuristics);
            if(tile != null)
                topTiles.add(tile);
            counter++;
        }

        Collections.shuffle(topTiles);
        return topTiles.get(0);
    }

    private boolean tileAlreadySelected(ArrayList<LandTile> topTiles, int row, int col){
        for(LandTile tile : topTiles){
            if(tile.getRow() == row && tile.getCol() == col)
                return true;
        }
        return false;
    }

    private LandTile getTileWithMaxHeuristic(ArrayList<LandTile> topTiles, int[][] heuristics){
        int row=0, col=0, max = 0;
        LandTile selectedTile = null;
        for(int i = 0; i < heuristics.length; i++) {
            for (int j = 0; j < heuristics[i].length; j++) {
                if( !tileAlreadySelected(topTiles, i, j) && heuristics[i][j] > max){
                    max = heuristics[i][j];
                    row = i;
                    col = j;
                }
            }
        }
        if(max > 0){
            try {
                selectedTile = (LandTile) Board.getInstance().getTile(row, col);
            }catch (Exception e){
                //Cannot occur
            }
        }
        return selectedTile;
    }

    private int[][] getMoveHeuristics(){
        int[][] heuristics = getOverviewOfMovablePawns();
        if( wantAggressivePlay() )
            heuristics = updateHeuristicsAggressive(heuristics);
        else
            heuristics = updateHeuristicsDefensive(heuristics);
        return heuristics;
    }

    private int[][] updateHeuristicsAggressive(int[][] heuristics){
        //First part of game -> attach -> first lines are need to be move first
        //assumption computer player is on top
        int multiplicationFactor = 0;
        for(int i = 0; i < heuristics.length; i++){
            multiplicationFactor++;
            for(int j = 0; j < heuristics[i].length; j++){
                heuristics[i][j] *= multiplicationFactor;
            }
        }
        return heuristics;
    }

    private int[][] updateHeuristicsDefensive(int[][] heuristics){
        //last part of game -> defense -> first lines are need to be move first
        //assumption computer player is on top
        int multiplicationFactor = 0;
        for(int i = heuristics.length-1; i >= 0; i--){
            multiplicationFactor++;
            for(int j = 0; j < heuristics[i].length; j++){
                heuristics[i][j] *= multiplicationFactor;
            }
        }
        return heuristics;
    }

    private int[][] getOverviewOfMovablePawns(){
        //Returns a board representation of moveable pawns. 0 = cannot move, 1 = can move
        int[][] boardMoveHeuristics = new int[Board.getNumberRows()][Board.getNumberCols()];
        ArrayList<LandTile> tilesWithPawns = Board.getInstance().getTilesWithPawnsOfColor(getColor());
        Move move = new Move(getColor());
        for (LandTile tile : tilesWithPawns) {
            if(tile.getPawn().isMovable() &&
                    move.canMoveDownFrom(tile) || move.canMoveLeftFrom(tile) ||
                    move.canMoveRightFrom(tile) || move.canMoveUpFrom(tile)){
                boardMoveHeuristics[tile.getRow()][tile.getCol()] = 1;
            }
        }
        return boardMoveHeuristics;
    }

    private double getPercentageArmyAlive(){
        int curArmySize = army.getArmy().size();
        int intialArmySize = Army.NUMBER_PAWNS;
        return curArmySize / (double) intialArmySize;
    }

    private boolean wantAggressivePlay(){
        return getPercentageArmyAlive() > 0.5;
    }

    private enum Direction{UP, DOWN, LEFT, RIGHT}

}
