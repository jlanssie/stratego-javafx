package Stratego.Model;

public class MoveHistory {
    private LandTile previousOrigin;
    private LandTile previousDestination;
    private int numberRepetitions;
    private final int MAX_NUMBER_OF_REPETITION = 5;

    public boolean moveRepeatedTooOften(Move newMove){
        LandTile newOrigin = newMove.getOriginTile();
        LandTile newDestination = newMove.getDestinationTile();

        updateNumberOfRepetitions(newOrigin, newDestination);

        if (numberRepetitions > MAX_NUMBER_OF_REPETITION){
            return true;
        } else {
            updatePreviousOriginAndDestination(newOrigin, newDestination);
        }

        return false;
    }

    private void updateNumberOfRepetitions(LandTile newOrigin, LandTile newDestination){
        if (isOppositeMove(newOrigin, newDestination)) {
            numberRepetitions++;
        } else {
            numberRepetitions = 0;
        }
    }

    private boolean isOppositeMove(LandTile newOrigin, LandTile newDestination){
        if(previousOrigin == null || previousDestination == null ||
                newOrigin == null || newDestination == null)
            return false;

        return (newDestination.equals(previousOrigin)) && (newOrigin.equals(previousDestination));
    }

    private void updatePreviousOriginAndDestination(LandTile newOrigin, LandTile newDestination){
        previousOrigin = newOrigin;
        previousDestination = newDestination;
    }
}
