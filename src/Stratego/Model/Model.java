package Stratego.Model;

import Stratego.Model.Exceptions.CorruptedFileException;
import Stratego.View.MainScreen.MainScreenPresenter;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Model {
    private static Board board;
    private static Scanner keyboard = new Scanner(System.in);
    private static Player player1;
    private static Player player2;

    public static void startNewGame(String playerName, PawnPlacement placementMethod){
        startNewGame(playerName, placementMethod, null);
    }

    public static void startNewGame(String playerName, PawnPlacement placementMethod, File file){
        board = Board.getInstance();
        board.clearBoard();
        initialiseComputerPlayer();
        if(file == null)
            initialiseHumanPlayer(playerName, placementMethod);
        else
            initialiseHumanPlayer(playerName, placementMethod, file);
    }

    private static void initialiseComputerPlayer(){
        player1 = new ComputerPlayer(Color.RED, Player.PlaceOnBoard.TOP);
        player1.placePawns();
    }

    private static void initialiseHumanPlayer(String name, PawnPlacement placementMethod){
        initialiseHumanPlayer(name, placementMethod, null);
    }

    private static void initialiseHumanPlayer(String name, PawnPlacement placementMethod, File file){
        player2 = new Player(name, Color.BLUE, Player.PlaceOnBoard.BOTTOM);
        PawnSetup pawnSetup = new PawnSetup(player2.army, player2.place);

        if(placementMethod == PawnPlacement.RANDOM)
            pawnSetup.createRandomSetup();
        else if(placementMethod == PawnPlacement.SMARTRANDOM)
            pawnSetup.createSmartRandomSetup();
        else if(placementMethod == PawnPlacement.LOADSETUP){
            try {
                pawnSetup.loadSetupFromFile(file);
            } catch (FileNotFoundException e) {
                System.out.println("\tThe provided file does not exist");
            } catch (CorruptedFileException e) {
                System.out.println("\t" + e.getMessage());
            }
        }
    }

    public static void main(String[] args) {

        boolean hasWon = false;
        Player currentPlayer = player1;
        Player otherPlayer = getOtherPlayer(currentPlayer);

        do {
            System.out.println(currentPlayer.getName() + "'s turn.");

            if (currentPlayer == getComputerPlayer()){Move move = currentPlayer.makeMove(); move.movePawn();} else {currentPlayer.makeMove();}
            currentPlayer.increaseScore();

            System.out.printf("\nScore of %s: %d%n", currentPlayer.getName(), currentPlayer.getScore());
            System.out.printf("Score of %s: %d%n", otherPlayer.getName(), otherPlayer.getScore() );

            hasWon = didUserWinFrom(otherPlayer);
            if(hasWon) {
                System.out.printf("\n\nCongratulations %s, you have won the game!", currentPlayer.getName());
            } else {
                System.out.println("\nThis is the board: \n" + board.toString());
                otherPlayer = currentPlayer;
                currentPlayer = getOtherPlayer(currentPlayer);
            }
        } while(!hasWon);
    }

    private static Player getOtherPlayer(Player currentPlayer) {
        return currentPlayer == player1 ? player2 : player1;
    }

    public static Player getPlayerWithColor(Color color) {
        return player1.getColor() == color ? player1 : player2;
    }

    private static Player initPlayer(Color color, Player.PlaceOnBoard place) {
        System.out.print("Player: what's your name?\t");
        String name = keyboard.nextLine();
        return new Player(name, color, place);
    }

    public static boolean didUserWinFrom(Player otherPlayer) {
        if(! otherPlayer.getArmy().canStillMove()) { return true; }
        if(otherPlayer.getArmy().isFlagCaptured()) { return true; }
        return  false;
    }

    public static void showRules() {
        System.out.println("Aan het begin van het spel staan een rood leger en een blauw leger tegenover elkaar; elk bestaand uit 40 stukken in verschillende rangen." +
                "De bedoeling is dat de vlag van de tegenstander wordt veroverd of dat de tegenstander niet meer kan bewegen. " +
                "Stukken mogen hoogstens 5 keer heen en weer bewegen; daarna moet de speler een andere zet doen.\n" +
                "Rood begint. Alle stukken mogen per beurt 1 veld horizontaal of verticaal op het bord bewegen. " +
                "Uitzonderingen zijn de verkenner, die zowel horizontaal als verticaal zo ver mag bewegen als mogelijk, en de vlag en de bommen die niet mogen bewegen. " +
                "Stukken kunnen niet over elkaar heen springen.\n" +
                "De stukken staan zodanig opgesteld dat de speler aan de andere kant van het bord niet kan zien waar welk stuk van de tegenstander staat. " +
                "Alleen als een stuk een ander stuk aanvalt – door op hetzelfde vak te stappen – wordt de rang van beide stukken bekendgemaakt. " +
                "Over het algemeen geldt dat het hoogste stuk wint, en het andere van het bord wordt gehaald. Als beide even hoog zijn, worden beide verwijderd.\n" +
                "Hierop zijn twee uitzonderingen:\n" +
                "- als de spion, die de laagste in rang is van alle stukken, het hoogste stuk aanvalt (de maarschalk), dan wordt de maarschalk juist wel verslagen. Valt de maarschalk echter de spion aan, dan verdwijnt de spion van het bord.\n" +
                "- een bom wordt alleen weggenomen als deze door een mineur wordt aangevallen.\n" +
                "De bommen en de vlag mogen na plaatsing niet meer verplaatst worden. Een bom wint van elk stuk dat het probeert te slaan, met uitzondering van de mineur. De vlag verliest van elke rang en het veroveren ervan is tevens het doel van het spel: de speler die als eerste de vlag van de tegenstander aantikt met een willekeurig rang, heeft gewonnen.");
    }

    public static Player getComputerPlayer(){
        return player1;
    }

    public static Player getHumanPlayer() {
        return player2;
    }
}
