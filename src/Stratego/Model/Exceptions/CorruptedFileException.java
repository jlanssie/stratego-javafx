package Stratego.Model.Exceptions;

public class CorruptedFileException extends Exception{
    public CorruptedFileException() { }
    public CorruptedFileException(String message) {
        super(message);
    }

    //TODO for all error messages: Show in GUI an an Alertbox.
}
