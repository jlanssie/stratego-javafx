package Stratego.Model.Exceptions;

public class IllegalPawnPlacement extends Exception{
    public IllegalPawnPlacement() { }
    public IllegalPawnPlacement(String message) {
        super(message);
    }
}