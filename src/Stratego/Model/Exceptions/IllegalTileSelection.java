package Stratego.Model.Exceptions;

public class IllegalTileSelection extends Exception {
    public IllegalTileSelection() {
        super();
    }

    public IllegalTileSelection(String message) {
        super(message);
    }
}
