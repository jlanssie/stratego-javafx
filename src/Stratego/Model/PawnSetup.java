package Stratego.Model;

import Stratego.Model.Exceptions.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

public class PawnSetup {

    private static Scanner keyboard = new Scanner(System.in);
    private static Board board = Board.getInstance();
    private static Random random = new Random();
    private Army army;
    private Player.PlaceOnBoard place;
    private int firstRow = 3;
    private int lastRow = 0;

    public PawnSetup(Army army, Player.PlaceOnBoard place){
        this.army = army;
        this.place = place;
        if(place == Player.PlaceOnBoard.BOTTOM){
            firstRow = 6;
            lastRow = Board.getNumberRows()-1;
        }
    }
/*
    public void createManualSetup() {
        ArrayList<Pawn> pawns = army.cloneArmy();
        Pawn pawn;

        do{
            System.out.println("\nPlace a pawn");
            printAvailablePawns(pawns);
            System.out.print("\n\tGive the rank of the pawn you want to place: ");

            try {
                int rank = keyboard.nextInt();
                pawn = getPawnWithRank(pawns, rank);
                Tile tile = askOnWhichTileThePawnNeedsToBePlaced();

                if(isValidTile(tile)) {
                    placePawn(pawn, tile.getRow(), tile.getCol());
                    pawns.remove(pawn);
                    System.out.println("\n\nCurrent board: "+ board.toString());
                }

            } catch (IllegalTileSelection e) {
                System.out.print("Invalid move: ");
                System.out.println(e.getMessage());
                System.out.println("Please try again..");
            }

        } while(pawns.size() > 0);
    }
 */

    public void createRandomSetup(){
        ArrayList<Pawn> pawns = army.getArmy();
        placePawnsRandom(pawns);
    }

    public void loadSetupFromFile(String filepath) throws FileNotFoundException, CorruptedFileException {
        FileReader fileReader = new FileReader(filepath);
        loadSetupFromFile(fileReader);
    }

    public void loadSetupFromFile(File file) throws FileNotFoundException, CorruptedFileException {
        FileReader fileReader = new FileReader(file);
        loadSetupFromFile(fileReader);
    }

    public void loadSetupFromFile(FileReader fileReader) throws FileNotFoundException, CorruptedFileException {
        ArrayList<int[]> lines;
        ArrayList<Pawn> pawns = army.cloneArmy();

        //Open en read the file
        lines = fileReader.readFile();

        if(lines.size() != 40)
            throw new CorruptedFileException("The file should contain 40 pawns, but contains " + lines.size());

        for(int[] line : lines){
            int row = transformRowToCorrectPlace(line[0]);
            int col = line[1];
            int rank = line[2];
            Pawn pawn = getPawnWithRank(pawns, rank);
            placePawn(pawn, row, col);
            pawns.remove(pawn);
        }
    }

    public static boolean isValidSetupFile(File file){
        FileReader fileReader = new FileReader(file);

        ArrayList<int[]> lines;
        Player tmpPlayer = new Player("tmpPlayer", Color.RED, Player.PlaceOnBoard.TOP); //Hack om aan army te geraken
        ArrayList<Pawn> pawns = tmpPlayer.getArmy().cloneArmy();

        //Open en read the file
        try {
            lines = fileReader.readFile();
        } catch (Exception e) {
            return false;
        }

        if(lines.size() != 40)
            return false;

        for(int[] line : lines){
            int rank = line[2];
            Pawn pawn;
            try {
                pawn = getPawnWithRank(pawns, rank);
            }catch (NullPointerException e){
                return false;
            }
            pawns.remove(pawn);
        }
        return true;
    }

    private int transformRowToCorrectPlace( int row){
        if( place == Player.PlaceOnBoard.TOP && row <= 3)
            return row;
        if( place == Player.PlaceOnBoard.BOTTOM && row >= 6)
            return row;

        switch (row){
            case 0: return 9;
            case 1: return 8;
            case 2: return 7;
            case 3: return 6;
            case 6: return 3;
            case 7: return 2;
            case 8: return 1;
            case 9: return 0;
        }
        return -1;
    }

    public void createSmartRandomSetup() {
        ArrayList<Pawn> pawns = army.cloneArmy();

        smartPlaceFlag(pawns);
        smartPlaceBombsAroundFlag(pawns);
        smartPlacePawnsWithRankLowerThen3(pawns);
        smartPlaceSpy(pawns);
        smartPlaceSomeScouts(pawns);
        placePawnsRandom(pawns);
    }

    private void placePawnsRandom(ArrayList<Pawn> pawns){
        Tile[][] tiles = Board.getInstance().getTiles();
        int counter = 0;

        //Shuffle remaining pawns
        Collections.shuffle(pawns);

        //Place remaining pawns
        int startRow = firstRow < lastRow ? firstRow : lastRow;
        int endRow = firstRow < lastRow ? lastRow : firstRow;
        for(int i = startRow; i <= endRow; i++){
            for(int j = 0; j < tiles[0].length; j++){
                Tile tile = tiles[i][j];
                if(isValidTile(tile))
                    ((LandTile) tile).setPawn(pawns.get(counter++));
            }
        }
    }

    private void placePawnAndRemovePawnFromList(ArrayList<Pawn> pawns, Pawn pawn, int row, int col) throws IllegalTileSelection{
        LandTile tile = (LandTile) board.getTile(row, col);
        placePawnAndRemovePawnFromList(pawns, pawn, tile);
    }

    private void placePawnAndRemovePawnFromList(ArrayList<Pawn> pawns, Pawn pawn, LandTile tile){
        tile.setPawn(pawn);
        pawns.remove(pawn);
    }

    private void smartPlaceSomeScouts(ArrayList<Pawn> pawns){
        ArrayList<Pawn> scouts = new ArrayList<>();
        scouts.add(getPawnWithRank(pawns, Army.PAWN_TYPES.SCOUT.rank));
        scouts.add(getPawnWithRank(pawns, Army.PAWN_TYPES.SCOUT.rank, 2));
        scouts.add(getPawnWithRank(pawns, Army.PAWN_TYPES.SCOUT.rank, 3));
        scouts.add(getPawnWithRank(pawns, Army.PAWN_TYPES.SCOUT.rank, 4));
        scouts.add(getPawnWithRank(pawns, Army.PAWN_TYPES.SCOUT.rank, 5));

        int topRow = place == Player.PlaceOnBoard.TOP ? 3 : 6;
        int middelRow = place == Player.PlaceOnBoard.TOP ? 2 : 7;

        for (Pawn scout : scouts) {
            boolean placed = false;
            while(!placed){
                double chance = random.nextDouble();
                int rowToPlace = chance < 0.65 ? topRow : middelRow;
                int colToPlace = getRandomNumberBetween(0, Board.getNumberCols());
                try {
                    LandTile tile = (LandTile) board.getTile(rowToPlace, colToPlace);
                    if (isValidTile(tile)) {
                        placePawnAndRemovePawnFromList(pawns, scout, rowToPlace, colToPlace);
                        placed = true;
                    }
                }catch (IllegalTileSelection e){
                    //Do nothing, next iteration will handle this
                }
            }
        }

    }

    private void smartPlaceSpy(ArrayList<Pawn> pawns){
        // Place pawn random on a tile in the middle rows
        Pawn spy = getPawnWithRank(pawns, Army.PAWN_TYPES.SPY.rank);
        int middelRow1 = place == Player.PlaceOnBoard.TOP ? 2 : 7;
        int middelRow2 = place == Player.PlaceOnBoard.TOP ? 1 : 8;
        boolean placed = false;
        while(!placed){
            double chance = random.nextDouble();
            int rowToPlace = chance < 0.5 ? middelRow1 : middelRow2;
            int colToPlace = getRandomNumberBetween(0, Board.getNumberCols());
            try {
                LandTile tile = (LandTile) board.getTile(rowToPlace, colToPlace);
                if (isValidTile(tile)) {
                    placePawnAndRemovePawnFromList(pawns, spy, rowToPlace, colToPlace);
                    placed = true;
                }
            }catch (IllegalTileSelection e){
                //Do nothing, next iteration will handle this
            }
        }

    }

    private void smartPlacePawnsWithRankLowerThen3(ArrayList<Pawn> pawns){
        // Maarschalk, general and colonels are placed randomnly on the middle 2 rows
        // Equally divided between the colomns
        ArrayList<Pawn> highRankedPawns = getPawnsWithRanksLowerThan3(pawns);
        Collections.shuffle(highRankedPawns);
        int middelRow1 = place == Player.PlaceOnBoard.TOP ? 2 : 7;
        int middelRow2 = place == Player.PlaceOnBoard.TOP ? 1 : 8;
        int rowToPlace, colToPlace;
        int[][] colborders = { {0,2}, {3,6}, {7,9}, {0,9} }; //make sure they are equally divided between columns
        int counter = 0;
        LandTile tile;
        for (Pawn highRankedPawn : highRankedPawns) {
            boolean placed = false;
            while(!placed){
                double chance = random.nextDouble();
                rowToPlace = chance < 0.5 ? middelRow1 : middelRow2;
                colToPlace = getRandomNumberBetween(colborders[counter][0], colborders[counter][1] ); //make sure they are equally divided between columns
                try {
                    tile = (LandTile) board.getTile(rowToPlace, colToPlace);
                    if (isValidTile(tile)) {
                        placePawnAndRemovePawnFromList(pawns, highRankedPawn, tile);
                        placed = true;
                    }
                }catch (IllegalTileSelection e){
                    //Do nothing -> reselect a new tile
                }
            }
            counter++;
        }
    }

    private int getRandomNumberBetween(int min, int max){
        return random.nextInt(max - min + 1) + min;
    }

    private void smartPlaceFlag(ArrayList<Pawn> pawns){
        //Places the flag in the last or 2nd last row and in random column
        try {
            Pawn flag = getPawnWithRank(pawns, Army.PAWN_TYPES.FLAG.rank);
            int flagRow = place == Player.PlaceOnBoard.TOP ? lastRow + random.nextInt(2) : lastRow - random.nextInt(2);
            int flagCol = random.nextInt(10);
            placePawnAndRemovePawnFromList(pawns, flag, flagRow, flagCol);
        } catch (NullPointerException | IllegalTileSelection e) {
            //Should not occur
            e.printStackTrace();
        }
    }

    private void smartPlaceBombsAroundFlag(ArrayList<Pawn> pawns){
        Tile flagTile = board.getTileWithPAwn(Army.PAWN_TYPES.FLAG.rank, pawns.get(0).getColor());
        int flagRow = flagTile.getRow();
        int flagCol = flagTile.getCol();
        Pawn bomb;
        double placeChance = 0.9;
        double chance;
        int bombRow = -1;
        int bombCol = -1;

        //place bomb above flag
        try {
            bomb = getPawnWithRank(pawns, Army.PAWN_TYPES.BOMB.rank);
            bombRow = place == Player.PlaceOnBoard.TOP ? flagRow + 1 : flagRow - 1;
            bombCol = flagCol;
            placePawnAndRemovePawnFromList(pawns, bomb, bombRow, bombCol);
        }catch (IllegalTileSelection | NullPointerException e){
            //Should never occur
            System.out.println("row = " + bombRow + ". col = " + bombCol);
            e.printStackTrace();
        }

        //Place bomb left of flag with a chance of 0.9 percent
        try {
            chance = random.nextDouble();
            if (chance <= placeChance) {
                bomb = getPawnWithRank(pawns, Army.PAWN_TYPES.BOMB.rank);
                bombRow = flagRow;
                bombCol = flagCol - 1;
                placePawnAndRemovePawnFromList(pawns, bomb, bombRow, bombCol);
            }
        }catch (NullPointerException e){
            //Should never occur
            e.printStackTrace();
        }catch (IllegalTileSelection e){
            //Do nothing; only thrown if flag is at the boarder of the playboard
        }

        //Place bomb right of flag with a chance of 0.9 percent
        try {
            chance = random.nextDouble();
            if (chance <= placeChance) {
                bomb = getPawnWithRank(pawns, Army.PAWN_TYPES.BOMB.rank);
                bombRow = flagRow;
                bombCol = flagCol + 1;
                placePawnAndRemovePawnFromList(pawns, bomb, bombRow, bombCol);
            }
        }catch (NullPointerException e){
            //Should never occur
            e.printStackTrace();
        }catch (IllegalTileSelection e){
            //Do nothing; only thrown if flag is at the boarder of the playboard
        }

        //Place bomb under the flag with a chance of 0.9 percent
        try {
            chance = random.nextDouble();
            if (chance <= placeChance) {
                bomb = getPawnWithRank(pawns, Army.PAWN_TYPES.BOMB.rank);
                bombRow = place == Player.PlaceOnBoard.TOP ? flagRow - 1 : flagRow + 1;
                bombCol = flagCol;
                placePawnAndRemovePawnFromList(pawns, bomb, bombRow, bombCol);
            }
        }catch (NullPointerException e){
            //Should never occur
            e.printStackTrace();
        }catch (IllegalTileSelection e){
            //Do nothing; only thrown if flag is at the boarder of the playboard
        }
    }

    private Tile askOnWhichTileThePawnNeedsToBePlaced() throws IllegalTileSelection {
        System.out.print("\tGive the coordinates where you want to place the pawn (format i,j): ");
        keyboard.nextLine(); //Otherwise next lines returns an empty string, and doesn't wait for input
        String coordinates = keyboard.nextLine();
        int row = getRow(coordinates);
        int col = getCol(coordinates);
        return board.getTile(row,col);
    }

    private boolean isValidTile(Tile tile){
        if( !(tile instanceof LandTile))
            return false;

        if(!tile.isEmtpy())
            return false;

        return doesTileBelongToUser(tile);
    }

    private static int getRow(String coordinateString){
        String[] tmp = coordinateString.split(",");
        int row = Integer.parseInt(tmp[0]);
        return row;
    }

    private static int getCol(String coordinateString){
        String[] tmp = coordinateString.split(",");
        int col = Integer.parseInt(tmp[1]);
        return col;
    }

    private ArrayList<Pawn> getPawnsWithRanksLowerThan3(ArrayList<Pawn> pawns){
        //Place higher ranks (maarschalk -> major) on middle 2 rows and equally divided between columns
        ArrayList<Pawn> highRankedPawns = new ArrayList<>();
        highRankedPawns.add(getPawnWithRank(pawns, 1));
        highRankedPawns.add(getPawnWithRank(pawns, 2));
        highRankedPawns.add(getPawnWithRank(pawns, 3));
        highRankedPawns.add(getPawnWithRank(pawns, 3, 2));
        return highRankedPawns;
    }

    private static Pawn getPawnWithRank(ArrayList<Pawn> pawns, int rank, int nbOccurence) throws NullPointerException {
        int curOccurence = 0;
        for(Pawn pawn : pawns){
            if(pawn.getRank() == rank) {
                curOccurence++;
                if(nbOccurence == curOccurence)
                    return pawn;
            }
        }
        throw new NullPointerException("There are no more pawns with rank " + rank + " available.");
    }

    private static Pawn getPawnWithRank(ArrayList<Pawn> pawns, int rank) throws NullPointerException {
        return getPawnWithRank(pawns, rank, 1);
    }

    private void printAvailablePawns(ArrayList<Pawn> pawns) {
        StringBuilder available = new StringBuilder();
        available.append("\tAvailable pawns: ");
        for (int i = 0; i < pawns.size(); i++) {
            Pawn pawn =  pawns.get(i);
            available.append(pawn.getName()).append("(rank=").append(pawn.getRank()).append(")");
            if(i < pawns.size()-1) {
                available.append(", ");
            }
        }
        System.out.print(available.toString());
    }

    private void placePawn(Pawn pawn, int row, int col) {
        LandTile tile = null;

        try {
            tile = ((LandTile) board.getTile(row,col));
        } catch (IllegalTileSelection e) {
            System.out.println(e.getMessage() + "\n" + e.getStackTrace());
        }

        tile.setPawn(pawn);
    }

    private boolean doesTileBelongToUser(Tile tile){
        int row = tile.getRow();
        int col = tile.getCol();

        if(place == Player.PlaceOnBoard.TOP && (row < lastRow || row > firstRow))
            return false;
        if(place == Player.PlaceOnBoard.BOTTOM && (row < firstRow || row > lastRow))
            return false;
        if(col < 0 || col >= board.NUMBER_ROWS)
            return false;
        return true;
    }

    String[] getPresetFilepaths(){
        File directory = new File("resources/presets/"); //Put correct path
        File[] presetFiles = directory.listFiles();
        String[] filepaths = new String[presetFiles.length];
        int counter = 0;
        for(File presetFile: presetFiles){
            filepaths[counter++] = presetFile.getAbsolutePath();
        }
        return filepaths;
    }

}
