package Stratego.Model;

public class WaterTile extends Tile {
    private int row;
    private int col;

    public WaterTile(int row, int col) {
        super(row, col);
    }

    @Override
    public boolean isAccessible(){
        return false; //Any tile can be accessed, except water tiles
    }

    @Override
    public String toString(){
        return "  W ";
    }

    @Override
    public boolean isEmtpy(){
        return true;
    };
}
