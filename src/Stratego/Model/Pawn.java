package Stratego.Model;

public class Pawn implements Comparable<Pawn>{
    private String name;
    private int rank;
    private int maxSteps;
    private Color color;

    Pawn(String name, int rank, int maxSteps, Color color) {
        this.name = name;
        this.rank = rank;
        this.maxSteps = maxSteps;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public int getRank(){
        return rank;
    }

    public Color getColor(){
        return color;
    }

    public int getMaxSteps() {
        return maxSteps;
    }

    public boolean canMoveNumberOfSteps(int numberOfSteps){
        return numberOfSteps <= getMaxSteps();
    }

    public boolean isMovable(){
        return getMaxSteps() > 0;
    }

    @Override
    public int compareTo(Pawn pawn) {
        //Always use <this> for the attacker & the parameter pawn for the defender!!!
        //Returns positive number if this pawn (attacker) kills the parameter pawn (defender)
        //Returns a negative number if the parameter pawn (defender) kills this pawn (attacker)
        //Returns a 0 if both are killed
        if(this.getRank() == Army.PAWN_TYPES.BOMB.rank && pawn.getRank() != Army.PAWN_TYPES.MINER.rank)
            return 1;
        if(this.getRank() != Army.PAWN_TYPES.MINER.rank && pawn.getRank() == Army.PAWN_TYPES.BOMB.rank)
            return -1;
        if(this.getRank() == Army.PAWN_TYPES.SPY.rank && pawn.getRank() == Army.PAWN_TYPES.MAARSCHALK.rank)
            return 1;
        if(pawn.getRank() == Army.PAWN_TYPES.FLAG.rank)
            return 1;

        return pawn.getRank() - this.getRank();
    }
}
