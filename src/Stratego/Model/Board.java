package Stratego.Model;

import Stratego.Model.Exceptions.IllegalTileSelection;

import java.util.ArrayList;

public class Board {
    private Tile[][] board;
    private static Board gameboard = null;
    final static int NUMBER_ROWS = 10;

    private Board() {
        this.board = new Tile[NUMBER_ROWS][NUMBER_ROWS];
        initializeBoard();
    }

    public static Board getInstance() {
        //Singleton; ensures we always return the same board and that there can only exist 1 board
        if(gameboard == null) {
            gameboard = new Board();
        }
        return gameboard;
    }

    public static int getNumberRows(){
        return NUMBER_ROWS;
    }

    public static int getNumberCols(){
        return NUMBER_ROWS;
    }

    public Tile[][] getTiles() {
        return board;
    }

    public void clearBoard(){
        for(int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if(!board[i][j].isEmtpy()){
                    LandTile landTile = (LandTile) board[i][j];
                    landTile.removePawn();
                }
            }
        }
    }

    public ArrayList<LandTile> getTilesWithPawnsOfColor(Color color){
        ArrayList<LandTile> tiles = new ArrayList<>();

        for(int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if(!board[i][j].isEmtpy()){
                    LandTile landTile = (LandTile) board[i][j];
                    if(landTile.getPawn().getColor().equals(color))
                        tiles.add(landTile);
                }
            }
        }

        return tiles;
    }

    public Tile getTile(int row, int col) throws IllegalTileSelection {
        if(row >= 0 && row < NUMBER_ROWS && col < NUMBER_ROWS && col >= 0) {
            return board[row][col];
        }
        throw new IllegalTileSelection("A tile with coordinates (" + row + "," + col +") does not exist");
    }

    public LandTile getTileWithPAwn(int rank, Color color){
        try {
            for (int i = 0; i < Board.getNumberRows(); i++) {
                for (int j = 0; j < Board.getNumberCols(); j++) {
                    if (getTile(i, j) instanceof LandTile) {
                        LandTile tile = (LandTile) getTile(i, j);
                        Pawn pawn = tile.getPawn();
                        if ( pawn != null && pawn.getRank() == rank && pawn.getColor() == color)
                            return tile;
                    }
                }
            }
        }catch (IllegalTileSelection e) {
            //cannot occur
        }
        return null;
    }

    private  void initializeBoard() {
        for(int i = 0; i < board.length; i++) {
            for(int j = 0; j < board[i].length; j++) {
                if( (i == 4 || i == 5) && (j == 2 || j == 3 || j == 6 || j ==7)) {
                    board[i][j] = new WaterTile(i,j);
                } else {
                    board[i][j] = new LandTile(i,j);
                }
            }
        }
    }

    @Override
    public String toString() {
        String separator = "    " + "-".repeat(50) + "\n";
        StringBuilder builder = new StringBuilder();

        // Col numbers
        builder.append("    ");
        for (int j = 0; j < board[0].length; j++) {
            builder.append(String.format(" %2d  ", j));
        }

        builder.append("\n").append(separator);
        for(int i = 0; i < board.length; i++) {
            builder.append(String.format("%-3d", i)).append("|");
            for (int j = 0; j < board[i].length; j++) {
                builder.append(board[i][j].toString()).append("|");
            }
            builder.append("\n").append(separator);
        }
        return builder.toString();
    }
}
