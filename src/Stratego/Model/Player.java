package Stratego.Model;

import Stratego.Model.Exceptions.*;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Player {
    private String name;
    protected Army army;
    private int score;
    protected PlaceOnBoard place;
    public enum PlaceOnBoard { TOP, BOTTOM }
    private Color color;
    private static Scanner keyboard = new Scanner(System.in);
    private MoveHistory moveHistory;

    public Player(String name, Color color, PlaceOnBoard place){
        this.name = name;
        this.army = new Army(color);
        this.place = place;
        this.color = color;
        score = 0;

        this.moveHistory = new MoveHistory();
    }

    public Color getColor() { return color; }

    public String getName() { return name; }

    public Army getArmy() { return army; }

    public int getScore() { return score; }

    public void addScore(int addToScore) { score+=addToScore; }

    public void increaseScore() { addScore(1); }

    public MoveHistory getMoveHistory() {
        return this.moveHistory;
    }

    public Move makeMove() {
        boolean validMove = false;
        Board board = Board.getInstance();
        LandTile originLandTile = null;
        LandTile destinationLandTile = null;
        Move move = null;

        System.out.printf("%s, it's your turn to make a move%n", name);
        do{
            try {

                move = new Move(this.getColor());

                System.out.print("Enter the origin tile (format: i,j): ");
                String originCoordinates = keyboard.nextLine();
                int originRow = getRow(originCoordinates);
                int originCol = getCol(originCoordinates);
                move.setOriginTile(originRow, originCol);

                System.out.print("Enter the destination tile (format: i,j): ");
                String destinationCoordinates = keyboard.nextLine();
                int destinationRow = getRow(destinationCoordinates);
                int destinationCol = getCol(destinationCoordinates);
                move.setDestinationTile(destinationRow, destinationCol);

                validMove = move.isValidMove();

                if(moveHistory.moveRepeatedTooOften(move))
                    throw new IllegalMoveException("You did this move 5 times, please select another move"); //Done so we can show this error in the GUI

            } catch (IllegalMoveException e) {
                validMove = false;
                System.out.println("\t" + e.getMessage());
                System.out.println("Try again to make a move:");
            }

        } while (!validMove );
        move.movePawn();
        return move;
    }

    private static int getRow(String coordinateString){
        String[] tmp = coordinateString.split(",");
        int row = Integer.parseInt(tmp[0]);
        return row;
    }

    private static int getCol(String coordinateString){
        String[] tmp = coordinateString.split(",");
        int col = Integer.parseInt(tmp[1]);
        return col;
    }

    public void placePawns(){
        boolean correctAnswer = true;
        PawnSetup pawnSetup = new PawnSetup(army, place);

        do {
            System.out.printf("%n%s, how do you want to place your pawns? M(anual), R(andom), A(I generated), F(ileImport): ", name);
            String antwoord = keyboard.nextLine();

            if (!antwoord.isEmpty()) {
                antwoord = antwoord.substring(0, 1).toUpperCase();

                switch (antwoord) {
                    /*
                    case "M":
                        pawnSetup.createManualSetup();
                        break;
                    */
                    case "R":
                        pawnSetup.createRandomSetup();
                        break;
                    case "A":
                        pawnSetup.createSmartRandomSetup();
                        break;
                    case "F":
                        try {
                            System.out.print("\tGive the path of the file you want to load: ");
                            String filepath = keyboard.nextLine();
                            pawnSetup.loadSetupFromFile(filepath);
                        } catch (FileNotFoundException e) {
                            System.out.println("\tThe provided file does not exist");
                            correctAnswer = false;
                        } catch (CorruptedFileException e) {
                            System.out.println("\t" + e.getMessage());
                            correctAnswer = false;
                        }
                        break;
                    default:
                        correctAnswer = false;
                        break;
                }
            }
        } while (!correctAnswer);
    }
}
